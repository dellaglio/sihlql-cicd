LATENCY_FILE_PATH=${1:-`pwd`}
for query in query-bd-np-evaluation query-bd-evaluation query-bdbr-np-evaluation query-bdbr-evaluation
do
    rm -f src/main/java/sihlmill/generated/Topology.java 
    mvn clean
    mvn compile -Dquery.path=queries/$query -Dlatency.file.path=$LATENCY_FILE_PATH'_'$query
    mvn package -Dquery.path=queries/$query -Dlatency.file.path=$LATENCY_FILE_PATH'_'$query
    for (( i=0; i<4; i++ ))
    do
	java -jar target/sihlmill-0.6.jar
    done
done


