[[_TOC_]]

# SihlMill and SihlQL

SihlMill is a query engine for SihlQL. SihlQL extends the SPARQL query syntax with various primitives for reading from data streams as well as processing them in a differentially private manner. Based on a SihlQL query, SihlMill builds a Flink topology that can be executed with Flink to process the specified data streams.

In the following, we explain how to install and run the execution engine. You can try it by: (1) importing the project in your IDE (we use IntelliJ as reference IDE, but we also tested and run it in Eclipse) or (2) executing it from command line (using Maven and bash). We assume that you have Java (8+) and Maven.

Please note that this is a research prototype (and by no means a commercially hardened system), which should not be used for publishing sensitive information anywhere. It does enable researchers to explore the nature privacy-preserving RDF stream publication.

## How the project is structured

We briefly summarise the design of SihlMill:  

1. Query compilation: the compiler gets a query as input, parses and optimises it using Apache Jena. Next, the compiler rewrites the query creating a Flink job. The result is the source code of a class named Topology.java in the package *sihlmill.generated.*
2. Job compilation: Topology.java should now be compiled. It is worth mentioning that this step is needed because it is not possible to generate **and** execute the topology in the same process, since Java raises problems about the dynamic types (e.g. Tuple and its derived classes in Flink).
3. The compiled job can be executed.

Going a bit more into technical details:

- Query compilation and job generation: pom.xml includes a plugin named _exec-maven-plugin_, which runs a Java main entry in the Maven compile phase.
  The compiler is in the class *Compiler*: the main method receives a SihlQL query file as one of the arguments. As explained above, *Compiler* generates the job *sihlmill.generated.Topology* which respresent the query in a Flink topology.
- Packaging: Maven packages the newly generated class *sihlmill.generated.Topology*  into the jar files: one that can be run as a standalone process, and one that can be submitted to a Flink cluster.

## The query language SihlQL

This is a sample SihlQL query:
```
PREFIX : <http://example.org/>
PREFIX s: <https://schema.org/>
ENABLE PRIVACY EPSILON 0.1 W 10
SELECT ?product (COUNT(?user) AS ?h)
FROM FILE STREAM <file:///tmp/stream.jsonld>
TO FILE <file:///tmp/output.out>
WHERE{
    ?review s:itemReviewed ?product ;
      s:author ?user .
}
GROUP BY ?product
```

One of the most important extensions of SihlQL is the `FROM` clause in its `FROM STREAM` and `FROM STATIC` variations. Using `FROM STREAM`, the source for the input stream can be defined to be a JSON-LD file (using `FROM FILE STREAM <file://[PATH_TO_FILE]>`), a Kafka topic (`FROM KAFKA STREAM <kafka://[BROKER_IP]> TOPIC <[TOPIC_NAME]>`) or a or a MQTT topic (`FROM MQTT STREAM <mqtt://[BROKER_IP]> TOPIC <[TOPIC_NAME]>`). 

`FROM STATIC` allows for an easy enrichment of the graphs with static RDF triples (e.g., sourcing like `FROM STATIC <file:///[PATH_TO_RDF_FILE]>`).

In addition to the possibility of reading from Kafka, SihlQL also adds a primitive for producing its results to a Kafka or MQTT topic. Using `TO KAFKA <kafka://[BROKER_IP]> TOPIC <[TOPIC_NAME]>` or `TO MQTT <mqtt://[BROKER_IP]> TOPIC <[TOPIC_NAME]>`, the resulting Flink job will produce all of its results to the corresponding Kafka or MQTT topic.

# How to run SihlMill
## Setting up

- Clone the project.

- Open the root directory *sihlmill*, where you will find the code and the data

- To initialise some example queries and unzip the data, run:

  ```sh ./setup.sh```

  You will find a new folder *queries*, and data will contain decompressed streams ready to be used to test SihlMill.


## Packaging the project from command line with Maven

* Go into the root dir ${project_root_path}, i.e., *sihlmill*, run the Maven command:

  ```sh
  mvn clean compile package -Dquery.path=${YOUR_QUERY_FILE_PATH}
  ```

  where YOUR_QUERY_FILE_PATH is the path to the query. We provide some example queries in ${project_root_path}/queries. When no query is provided, i.e. you run:

  ```sh
  mvn clean compile package
  ```

  the compiler will take a default query located at *${project_root_path}/queries/query-template*

* After the compilation, in the target directory, i.e., *${project_root_path}/target*, there will be three jars:
  * **sihlmill-0.6-jar-with-dependencies.jar**
    This jar contains the job with the dependencies. It can be executed via the Flink cluster.
  * **sihlmill-0.6-standalone.jar**
    This jar contains the job, also with dependencies. It is targeted to be executed via command line.
  * **original-sihlmill-0.6.jar**
    This jar contains the job without dependencies. It is targeted to be executed via the Flink cluster which has loaded the dependencies.

## Executing the project
### From Command Line

- Execute via command-line: 

```sh
cd ${project_root_path}/
java -jar target/sihlmill-0.6-standalone.jar
```

### From a Flink Cluster

- Upload the **sihlmill-0.6-jar-with-dependencies.jar** to the homepage __*http://JOBMANAGER_HOST:8081/#/submit*__ of the Flink cluster. As entry class, set: *sihlmill.generated.Topology*, then input the parallelism, and then click 'Submit'.
- Look at the terminal and wait for the result.

## From an IDE (IntelliJ)

In this section we describe how to run the project in IntelliJ.
#### Import the project using IntelliJ IDEA

- Open the project: at the up-left corner of the window of IntelliJ IDEA, click **File** -> click **Open** -> choose the root directory *sihlmill*  

At this point, the project will be opened. You should see the project in IntelliJ IDEA, like

![](src/main/resources/viewaftersetup.png)

* If you forgot to install the query parser package (see _Setting up_ section), you should do it, and then refresh the project. You can do that as in the figure:

![](src/main/resources/reimport.png)

* Your project is now ready to run!

#### Run the project in IntelliJ

- Write your own query in a file or chose one among the available ones (in the query folder)
- Configure the main entry of FlinkStream. Go to the **Run** menu -> Select **Edit Configurations**, then **Template**, and click **Application** :
  - Fill the **Main class** (as in Figure)
  - If the query is provided, fill it (otherwise will use the default one: *queries/query-template*)

At the end, the configuration page should be like this:

![](src/main/resources/run-ide.png).
- Open the class *sihlmill.compiler.FlinkStream*, open the right click menu and press **Run 'FlinkStream.main()'**
- The generated job is generated and stored in *sihlmill.generated.Topology*

It's time to run the Flink topology!

- Open the class *sihlmill.generated.Topology*,  open the right click menu, and press **Run 'Topology.main()'**
- Look at the terminal and wait for the result. For this double blind submission, we provide two sample streams and a data item (if you move them, you should adjust the path in the query files). At the beginning, the job loads the stream file in memory, and it could take some seconds (usually less than one minute). 

# More on the SihlMill and SihlQL

## Licence
The code of SihlMill is available under Apache 2.0 License.

## Publications
* D. Dell'Aglio and A. Bernstein: Differentially private stream processing for the semantic web. The Web Conference 2020 (TheWebConf 2020), ACM, pp. 1977-1987. Taipei, Taiwan, April 2020. 
* R. Pernischová, F. Ruosch, D. Dell'Aglio and A. Bernstein: Stream Processing: The Matrix Revolutions. 12th International Workshop on Scalable Semantic Web Knowledge Base Systems, co-located with the 17th International Semantic Web Conference, CEUR-WS.org, pp. 15-27. Monterey, USA, October 2018. 

## Contributors
* Daniele Dell'Aglio
* Romana Pernischova
* Florian Ruosch
* Roland Schaefli
* Pengcheng Duan
* Alessandro Margara

## Acknowledgements
SihlQL is partially supported by the ![Swiss National Science Foundation](http://www.snf.ch/) under contract number #407550_167177 (![NFP 75's](http://www.nfp75.ch/) project ![Privacy-preserving, stream analytics for non-computer scientists](http://www.nfp75.ch/en/projects/module-1-information-technology/project-boehlen))
