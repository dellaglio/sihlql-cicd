d=${1:-`pwd`}

unzip -o data/amazon.zip -d ./data

mkdir queries

cat > queries/query-bd-evaluation <<- EOM
PREFIX : <http://example.org/>
PREFIX s: <https://schema.org/>
ENABLE PRIVACY EPSILON 0.1 W 10
SELECT ?product (COUNT(?user) AS ?h)
FROM FILE STREAM <file://$d/data/streameval.jsonld>
FROM STATIC <test://localPath/data/products.ttl>
TO FILE <file://$d/output-bd-0.1-10.jsonld>
WHERE{
  GRAPH <test://localPath/data/products.ttl> {
    ?product a :AmazonProduct .
  }
  OPTIONAL {
    ?review s:itemReviewed ?product ;
      s:author ?user .
  }
}
GROUP BY ?product
EOM

cat > queries/query-bd-np-evaluation <<- EOM
PREFIX : <http://example.org/>
PREFIX s: <https://schema.org/>
SELECT ?product (COUNT(?user) AS ?h)
FROM FILE STREAM <file://$d/data/streameval.jsonld>
FROM STATIC <test://localPath/data/products.ttl>
TO FILE <file://$d/output-bd-np.jsonld>
WHERE{
	GRAPH <test://localPath/data/products.ttl> {
		?product a :AmazonProduct .
	}
	OPTIONAL {
		?review s:itemReviewed ?product ;
			s:author ?user .
	}
}
GROUP BY ?product
EOM

cat > queries/query-bdbr-evaluation <<- EOM
PREFIX : <http://example.org/>
PREFIX s: <https://schema.org/>
ENABLE PRIVACY EPSILON 5.0 W 10
SELECT ?product (COUNT(?user) AS ?h)
FROM FILE STREAM <file://$d/data/streameval.jsonld>
TO FILE <file://$d/output-bdbr-5-10.jsonld>
WHERE{
	?review s:itemReviewed ?product ;
		s:author ?user .
}
GROUP BY ?product
EOM

cat > queries/query-bdbr-np-evaluation <<- EOM
PREFIX : <http://example.org/>
PREFIX s: <https://schema.org/>
SELECT ?product (COUNT(?user) AS ?h)
FROM FILE STREAM <file://$d/data/streameval.jsonld>
TO FILE <file://$d/output-bdbr-np.jsonld>
WHERE{
	?review s:itemReviewed ?product ;
		s:author ?user .
}
GROUP BY ?product
EOM


