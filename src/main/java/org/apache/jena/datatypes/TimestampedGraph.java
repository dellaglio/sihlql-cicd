package org.apache.jena.datatypes;

import org.apache.jena.graph.Graph;

public class TimestampedGraph {
    private Graph graph;
    private long timestamp;

    public TimestampedGraph(Graph graph, long timestamp) {
        this.graph = graph;
        this.timestamp = timestamp;
    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
