package org.apache.jena.datatypes;

import org.apache.jena.graph.impl.LiteralLabel;

public class MatrixType extends BaseDatatype {
    public static final String theTypeURI = "urn:x-hp-dt:matrix";
    public static final RDFDatatype theMatrixType = new MatrixType();

    /** private constructor - single global instance */
    private MatrixType() {
        super(theTypeURI);
    }

    /**
     * Convert a value of this datatype out to lexical form.
     */
    public String unparse(Object value) {
        final Matrix m = (Matrix) value;
        return m.toString();
    }

    /**
     * Parse a lexical form of this datatype to a value
     *
     * @throws DatatypeFormatException
     *           if the lexical form is not legal
     */
    public Object parse(String lexicalForm) throws DatatypeFormatException {
        final int indexM = lexicalForm.indexOf("/n");

        if (indexM == -1) {
            throw new DatatypeFormatException(lexicalForm, theMatrixType, "");
        }
        try {
            final int indexN = (int) Math.ceil(lexicalForm.length() / (double) indexM);

            final Double[][] data = new Double[indexN][indexM];
            for (int i = 0; i < indexN; i++) {
                for (int j = 0; j < indexM; j++) {
                    final int index = lexicalForm.indexOf(" ");
                    data[i][j] = Double.parseDouble(lexicalForm.substring(0, index));
                    lexicalForm = lexicalForm.substring(index);
                }
            }

            return new Matrix(data);

        } catch (final NumberFormatException e) {
            throw new DatatypeFormatException(lexicalForm, theMatrixType, "");
        }
    }

    /**
     * Compares two instances of values of the given datatype. This does not allow
     * rationals to be compared to other number formats, Lang tag is not
     * significant.
     */
    public boolean isEqual(LiteralLabel value1, LiteralLabel value2) {
        if (value1.getDatatypeURI().equals(theTypeURI) && value2.getDatatypeURI().equals(theTypeURI)) {
            final Object o1 = parse(value1.getLexicalForm());
            final Object o2 = parse(value2.getLexicalForm());

            return o1.equals(o2);
        }

        return false;
    }
}
