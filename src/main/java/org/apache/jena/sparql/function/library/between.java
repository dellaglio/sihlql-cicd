package org.apache.jena.sparql.function.library;

import org.apache.jena.atlas.lib.Lib;
import org.apache.jena.datatypes.Matrix;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.query.QueryBuildException;
import org.apache.jena.sparql.expr.ExprList;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase;

import java.util.List;

/**
 * Returns a partial matrix defined by the indices.
 * Can handle different numbers of inputs.
 */
public class between extends FunctionBase {
    @Override
    public NodeValue exec(List<NodeValue> args) {
        if (args.size() > 2) {
            NodeValue v = args.get(0);

            if (v.isLiteral() && v.getDatatypeURI().equals(MatrixType.theTypeURI)) {
                Matrix m = new Matrix(v.asString());
                int row_from = 0;
                int row_to = m.getRowDimension();
                int col_from = 0;
                int col_to = m.getColumnDimension();

                if (args.size() == 3) {
                    row_from = Integer.parseInt(args.get(1).asString());
                    col_from = Integer.parseInt(args.get(2).asString());

                } else if (args.size() == 5) {
                    row_from = Integer.parseInt(args.get(1).asString());
                    row_to = Integer.parseInt(args.get(2).asString());
                    col_from = Integer.parseInt(args.get(3).asString());
                    col_to = Integer.parseInt(args.get(4).asString());
                }
                Matrix mb = m.getMatrix(row_from, row_to, col_from, col_to);

                return NodeValue.makeNode(mb.toString(), MatrixType.theMatrixType);
            }
        }

        return null;
    }

    @Override
    public void checkBuild(String uri, ExprList args) {
        if (args.size() != 3 && args.size() != 5) {
            throw new QueryBuildException("Function '" + Lib.className(this) + "' takes three or five arguments");
        }
    }
}
