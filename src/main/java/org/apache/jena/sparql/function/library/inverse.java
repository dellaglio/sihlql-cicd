package org.apache.jena.sparql.function.library;

import org.apache.jena.datatypes.Matrix;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase1;

/**
 * Returns the inverse of a matrix.
 */
public class inverse extends FunctionBase1 {

    @Override
    public NodeValue exec(NodeValue v) {
        if (v.isLiteral() && v.getDatatypeURI().equals(MatrixType.theTypeURI)) {
            final Matrix A = new Matrix(v.asString());
            return NodeValue.makeNode(A.inverse().toString(), MatrixType.theMatrixType);

        }
        return null;
    }

}
