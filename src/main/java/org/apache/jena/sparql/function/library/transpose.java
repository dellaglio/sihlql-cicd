package org.apache.jena.sparql.function.library;

import org.apache.jena.datatypes.Matrix;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase1;

/**
 * The tranpose of the matrix is returned
 */
public class transpose extends FunctionBase1 {
    @Override
    public NodeValue exec(NodeValue v) {
        if (v.isLiteral() && v.getDatatypeURI().equals(MatrixType.theTypeURI)) {
            Matrix m = new Matrix(v.asString());

            return NodeValue.makeNode(m.transpose().toString(), MatrixType.theMatrixType);
        }

        return null;
    }
}
