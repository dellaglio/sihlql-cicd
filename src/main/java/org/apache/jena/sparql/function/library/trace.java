package org.apache.jena.sparql.function.library;

import org.apache.jena.datatypes.Matrix;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase1;

/**
 * Returns the trace of the matrix, which is the sum of the diagonal.
 */
public class trace extends FunctionBase1 {

    @Override
    public NodeValue exec(NodeValue v) {
        if ((v.isLiteral() && v.getDatatypeURI().equals(MatrixType.theTypeURI))) {
            Matrix A = new Matrix(v.asString());
            return NodeValue.makeDouble(A.trace());
        }
        return null;
    }

}
