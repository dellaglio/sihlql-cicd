package org.apache.jena.sparql.function.library;

import org.apache.jena.datatypes.Matrix;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase2;

/**
 * Returns the result of a merge of two matrices.
 * If matrix A has an element at position i,j, it is added into the result matrix. If the element is empty, the element i,j of matrix B is added.
 */
public class merge extends FunctionBase2 {
    @Override
    public NodeValue exec(NodeValue v1, NodeValue v2) {
        if ((v1.isLiteral() && v1.getDatatypeURI().equals(MatrixType.theTypeURI)) && (v2.isLiteral() && v2.getDatatypeURI().equals(MatrixType.theTypeURI))) {
            Matrix A = new Matrix(v1.asString());
            Matrix B = new Matrix(v2.asString());

            Matrix C = A.merge(B);

            return NodeValue.makeNode(C.toString(), MatrixType.theMatrixType);
        }

        return null;
    }
}
