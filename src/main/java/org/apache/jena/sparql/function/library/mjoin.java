package org.apache.jena.sparql.function.library;

import org.apache.jena.datatypes.Matrix;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase3;

/**
 * Returns a join of two matrices based on the operator specified in a string.
 * It handles "+", "-", "*", and "/"
 */
public class mjoin extends FunctionBase3 {
    @Override
    public NodeValue exec(NodeValue v1, NodeValue v2, NodeValue op) {
        if ((v1.isLiteral() && v1.getDatatypeURI().equals(MatrixType.theTypeURI)) && (v2.isLiteral() && v2.getDatatypeURI().equals(MatrixType.theTypeURI))) {
            Matrix A = new Matrix(v1.asString());
            Matrix B = new Matrix(v2.asString());
            String o = op.asString();
            Matrix C = A.mjoin(B, o);

            return NodeValue.makeNode(C.toString(), MatrixType.theMatrixType);
        }

        return null;
    }
}
