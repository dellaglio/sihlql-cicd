package org.apache.jena.sparql.function.library;

import org.apache.jena.datatypes.Matrix;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase1;

/**
 * Returns the determinant of the input matrix.
 */
public class determinant extends FunctionBase1 {
    @Override
    public NodeValue exec(NodeValue v1) {
        if (v1.isLiteral() && v1.getDatatypeURI().equals(MatrixType.theTypeURI)) {
            Matrix A = new Matrix(v1.asString());

            return NodeValue.makeNodeDouble(A.det());
        }

        return null;
    }
}
