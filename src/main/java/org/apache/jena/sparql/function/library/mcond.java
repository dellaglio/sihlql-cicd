package org.apache.jena.sparql.function.library;

import org.apache.jena.datatypes.Matrix;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase1;

/**
 * Returns the result of the two norm condition of a matrix. This is the ratio of the smallest to the largest value in the matrix.
 */
public class mcond extends FunctionBase1 {

    @Override
    public NodeValue exec(NodeValue v) {
        if ((v.isLiteral() && v.getDatatypeURI().equals(MatrixType.theTypeURI))) {
            Matrix A = new Matrix(v.asString());
            return NodeValue.makeDouble(A.cond());
        }
        return null;
    }


}
