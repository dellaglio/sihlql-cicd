package org.apache.jena.sparql.algebra.op;

import org.apache.jena.graph.Node;
import org.apache.jena.sparql.algebra.Op;

public class OpStaticGraphMaterialised extends OpStaticGraph {
    private String name;

    public OpStaticGraphMaterialised(Node node, Op pattern, String name) {
        super(node, pattern);
        this.name = name;
    }

    @Override
    public String getName() {
        return "static_graph_materialised";
    }

    @Override
    public Op1 copy(Op newOp) {
        return new OpStaticGraphMaterialised(getNode(), newOp, name);
    }

    public String getGraphVarName() {
        return name;
    }
}
