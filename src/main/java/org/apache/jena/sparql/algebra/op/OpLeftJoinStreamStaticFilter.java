
package org.apache.jena.sparql.algebra.op;

import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.expr.ExprList;
import org.apache.jena.sparql.sse.Tags;

public class OpLeftJoinStreamStaticFilter extends OpLeftJoin {
    private OpLeftJoinStreamStaticFilter(Op left, Op right, ExprList expr) {
        super(left, right, expr);
    }

    public static Op create(Op left, Op right, ExprList expr) {
        if (left == null)
            return right;
        if (right == null)
            return left;
        return new OpLeftJoinStreamStaticFilter(left, right, expr);
    }

    @Override
    public String getName() {
        return "stream_static_" + Tags.tagLeftJoin + "_filt";
    }

    @Override
    public Op2 copy(Op newLeft, Op newRight) {
        return new OpLeftJoinStreamStaticFilter(newLeft, newRight, expressions);
    }

}
