
package org.apache.jena.sparql.algebra.op;

import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.sse.Tags;

public class OpJoinStaticStreamFlatMap extends OpJoin {
    private OpJoinStaticStreamFlatMap(Op left, Op right) {
        super(left, right);
    }

    public static Op create(Op left, Op right) {
        if (left == null)
            return right;
        if (right == null)
            return left;
        return new OpJoinStaticStreamFlatMap(left, right);
    }

    @Override
    public String getName() {
        return "static_stream_" + Tags.tagJoin + "_flatmap";
    }

    @Override
    public Op2 copy(Op newLeft, Op newRight) {
        return new OpJoinStaticStreamFlatMap(newLeft, newRight);
    }

}
