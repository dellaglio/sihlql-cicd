/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlql.demo;

import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class AmazonReviewsSourceFunction implements SourceFunction<Tuple4<Long, Node, Node, Node>> {
    boolean running = true;

    @Override
    public void cancel() {
        running = false;
    }

    @Override
    public void run(SourceContext<Tuple4<Long, Node, Node, Node>> context) throws Exception {
        Stream<String> stream = getStream();
        Iterator<String> iterator = stream.iterator();
        iterator.next(); //skip the first line
        while (running && iterator.hasNext()) {
            String s = iterator.next();
//			System.out.println(s);
            String[] data = s.split(",");
            String userUri = "http://example.org/user" + data[0];
            String productUri = "http://example.org/item" + data[1];
            String reviewUri = "http://example.org/review" + data[0] + "-" + data[1];
            String ratingUri = "http://example.org/rating" + data[0] + "-" + data[1];
            long timestamp = Long.parseLong(data[3]);
            context.collect(new Tuple4<>(
                    timestamp,
                    NodeFactory.createURI(reviewUri),
                    NodeFactory.createURI("https://schema.org/author"),
                    NodeFactory.createURI(userUri)
            ));
            context.collect(new Tuple4<>(
                    timestamp,
                    NodeFactory.createURI(reviewUri),
                    NodeFactory.createURI("https://schema.org/itemReviewed"),
                    NodeFactory.createURI(productUri)
            ));
            context.collect(new Tuple4<>(
                    timestamp,
                    NodeFactory.createURI(reviewUri),
                    NodeFactory.createURI("https://schema.org/reviewRating"),
                    NodeFactory.createURI(ratingUri)
            ));
            context.collect(new Tuple4<>(
                    timestamp,
                    NodeFactory.createURI(ratingUri),
                    NodeFactory.createURI("https://schema.org/ratingValue"),
                    NodeFactory.createLiteral(data[3], XSDDatatype.XSDdouble)
            ));
        }
    }

    private Stream<String> getStream() {
        try {
            ZipFile zipFile = new ZipFile("data/amazon.zip");

            Enumeration<? extends ZipEntry> en = zipFile.entries();
            while (en.hasMoreElements()) {
                ZipEntry ze = en.nextElement();
                if (ze.getName().contains("stream")) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(zipFile.getInputStream(ze), StandardCharsets.UTF_8));
                    return br.lines();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
