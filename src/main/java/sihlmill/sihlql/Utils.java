/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.sihlql;

import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.optimize.VariableUsageTracker;
import org.apache.jena.sparql.algebra.optimize.VariableUsageVisitor;
import org.apache.jena.sparql.algebra.walker.Walker;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.core.VarExprList;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Utils {
    public static Set<Var> extractVariables(Op op) {

        VariableUsageTracker vut = new VariableUsageTracker();
        Set<Var> ret = new HashSet<>();
        Walker.walk(op, new VariableUsageVisitor(vut) {
            @Override
            protected void action(String var) {
            }

            @Override
            protected void action(Var var) {
                ret.add(var);
            }

            @Override
            protected void action(Collection<Var> vars) {
                ret.addAll(vars);
            }
        });
        return ret;
    }

    public static Set<Var> extractJoinVariables(Op left, Op right) {
        Set<Var> var1 = extractVariables(left);
        Set<Var> var2 = extractVariables(right);
        var1.retainAll(var2);
        return var1;
    }

    public static Set<Var> extractVariables(VarExprList vel) {
        Set<Var> ret = new HashSet<>();
        for (Var v : vel.getVars()) {
            ret.add(v);
            ret.addAll(vel.getExpr(v).getVarsMentioned());
        }
        return ret;
    }
}
