/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.sihlql.rewriter;

import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.TransformCopy;
import org.apache.jena.sparql.algebra.Transformer;
import org.apache.jena.sparql.algebra.op.OpFilter;
import org.apache.jena.sparql.algebra.op.OpGraph;
import org.apache.jena.sparql.algebra.op.OpStaticGraph;
import org.apache.jena.sparql.algebra.optimize.Rewrite;

public class QLRewriter implements Rewrite {

    @Override
    public Op rewrite(Op op) {
        //FIXME: this one should happen inside Jena...
        op = Transformer.transform(new TransformCopy() {
            @Override
            public Op transform(OpGraph opGraph, Op subOp) {
                return new OpStaticGraph(opGraph.getNode(), subOp);
            }
            
        }, op);
        op = Transformer.transform(new TransformerLeftJoin(), op);
        op = Transformer.transform(new TransformerStreamStaticJoin(), op);
        return op;
    }
    


}
