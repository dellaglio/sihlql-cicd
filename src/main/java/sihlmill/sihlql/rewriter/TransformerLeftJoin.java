/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.sihlql.rewriter;

import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.TransformCopy;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.Var;

import sihlmill.sihlql.Utils;

import java.util.Set;

public class TransformerLeftJoin extends TransformCopy {
    @Override
    public Op transform(OpJoin opJoin, Op left, Op right) {
        if (left instanceof OpLeftJoin && right instanceof OpBGP) {
            return OpLeftJoin.create(
                    right,
                    ((OpLeftJoin) left).getRight(),
                    ((OpLeftJoin) left).getExprs()
            );
        } else if (left instanceof OpLeftJoin && right instanceof OpStaticGraph) {
            OpLeftJoin lj = (OpLeftJoin) left;
//			if(lj.getLeft() instanceof OpTable && lj.getRight() instanceof OpBGP) {
            //FIXME: check if this one is right:
            if (lj.getLeft() instanceof OpTable) {
                return OpLeftJoin.create(right, lj.getRight(), lj.getExprs());
            } else
                return super.transform(opJoin, left, right);
        } else if (right instanceof OpLeftJoin && left instanceof OpBGP) {
            return OpLeftJoin.create(
                    left,
                    ((OpLeftJoin) right).getRight(),
                    ((OpLeftJoin) right).getExprs()
            );
        } else if (right instanceof OpLeftJoin && left instanceof OpStaticGraph) {
            OpLeftJoin lj = (OpLeftJoin) right;
            if (lj.getLeft() instanceof OpTable && lj.getRight() instanceof OpBGP) {
                return OpLeftJoin.create(left, lj.getRight(), lj.getExprs());
            } else
                return super.transform(opJoin, right, left);
        } else if (left instanceof OpExtend && right instanceof OpBGP) {
            OpExtend var = (OpExtend) left;
            return var.copy(right);
        } else {
            return super.transform(opJoin, left, right);
        }
    }

    //Pushes extend down, but it would be better to push it up!
    @Override
    public Op transform(OpExtend opExtend, Op subOp) {
        if (!(subOp instanceof OpBGP)) {
            if (subOp instanceof Op2) {
                Op2 sop = ((Op2) subOp);
                Set<Var> leftVars = Utils.extractVariables(sop.getLeft());
                Set<Var> rightVars = Utils.extractVariables(sop.getRight());

                boolean left = false, right = false;
                for (Var v : Utils.extractVariables(opExtend.getVarExprList())) {
                    left = left || leftVars.contains(v);
                    right = right || rightVars.contains(v);
                }
                if (left && right) return super.transform(opExtend, subOp);
                else if (left && !right) {
                    OpExtend newOp = OpExtend.create(sop.getLeft(), opExtend.getVarExprList());
                    return sop.copy(newOp, sop.getRight());
                } else if (!left && right) {
                    OpExtend newOp = OpExtend.create(sop.getRight(), opExtend.getVarExprList());
                    return sop.copy(sop.getLeft(), newOp);
                }
            }
        }
        return super.transform(opExtend, subOp);
    }
    
    @Override
    public Op transform(OpGraph opGraph, Op subOp) {
        if (opGraph instanceof OpStaticGraph && subOp instanceof OpLeftJoin)
            return OpLeftJoin.create(
                    ((OpLeftJoin) subOp).getLeft(),
                    new OpStaticGraph(opGraph.getNode(), ((OpLeftJoin) subOp).getRight()),
                    ((OpLeftJoin) subOp).getExprs()
            );
        return super.transform(opGraph, subOp);
    }


}
