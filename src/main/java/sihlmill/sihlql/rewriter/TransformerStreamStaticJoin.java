/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.sihlql.rewriter;

import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.TransformCopy;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.Var;

import sihlmill.sihlql.Utils;

import java.util.Set;

public class TransformerStreamStaticJoin extends TransformCopy {
    @Override
    public Op transform(OpJoin opJoin, Op left, Op right) {
        Op staticOp = null;
        Op streamOp = null;
        if (left instanceof OpStaticGraph && !(right instanceof OpStaticGraph)) {
            staticOp = left;
            streamOp = right;
        } else if (right instanceof OpStaticGraph && !(left instanceof OpStaticGraph)) {
            staticOp = right;
            streamOp = left;
        } else {
            return super.transform(opJoin, left, right);
        }

        Set<Var> staticVars = Utils.extractVariables(staticOp);
        Set<Var> streamVars = Utils.extractVariables(streamOp);

        if (staticVars.containsAll(streamVars))
            return OpJoinStaticStreamFlatMap.create(staticOp, streamOp);
        else if (streamVars.containsAll(staticVars))
            return OpJoinStaticStreamFilter.create(staticOp, streamOp);
        else
            return OpJoinStaticStreamFlatMap.create(staticOp, streamOp);
    }

    @Override
    public Op transform(OpLeftJoin opLeftJoin, Op left, Op right) {
        Op staticOp = null;
        Op streamOp = null;
        if (left instanceof OpStaticGraph && !(right instanceof OpStaticGraph)) {
            staticOp = left;
            streamOp = right;

            Set<Var> staticVars = Utils.extractVariables(staticOp);
            Set<Var> streamVars = Utils.extractVariables(streamOp);

            return OpLeftJoinStaticStreamFlatMap.create(staticOp, streamOp, opLeftJoin.getExprs());
        } else if (right instanceof OpStaticGraph && !(left instanceof OpStaticGraph)) {
            staticOp = right;
            streamOp = left;

            Set<Var> staticVars = Utils.extractVariables(staticOp);
            Set<Var> streamVars = Utils.extractVariables(streamOp);

            if (staticVars.containsAll(streamVars))
                return OpLeftJoinStreamStaticFlatMap.create(streamOp, staticOp, opLeftJoin.getExprs());
            else if (streamVars.containsAll(staticVars))
                return OpLeftJoinStreamStaticFilter.create(streamOp, staticOp, opLeftJoin.getExprs());
            else
                return OpLeftJoinStreamStaticFlatMap.create(streamOp, staticOp, opLeftJoin.getExprs());
        } else {
            return super.transform(opLeftJoin, left, right);
        }

    }

}
