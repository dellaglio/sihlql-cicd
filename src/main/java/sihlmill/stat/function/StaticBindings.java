/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stat.function;

import org.apache.jena.graph.Node;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.binding.Binding;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//FIXME: we can make a smarter implementation...
public class StaticBindings implements Serializable {
    List<Var> vars;
    Set<Node[]> values;
    int size;

    public StaticBindings(List<Var> vars) {
        this.vars = vars;
        size = vars.size();
        values = new HashSet<>();
    }

    public void addBinding(Binding b) {
        Node[] item = new Node[size];
        int i = 0;
        for (Var var : vars) {
            item[i++] = b.get(var);
        }
        values.add(item);
    }

    public Set<Node> getProjection(Var var) {
        int pos = vars.indexOf(var);
        Set<Node> res = new HashSet<>();

        for (Node[] item : values) {
            res.add(item[pos]);
        }
        return res;
    }

    public List<Var> getVars() {
        return vars;
//		List<Var> ret = new ArrayList<>();
//		Collections.copy(ret, vars);
//		return ret;
    }

    public Set<Node[]> retrieve(Node[] item) {
        Set<Node[]> ret = new HashSet<>();
        if (item == null) {
            for (Node[] value : values) {
                ret.add(Arrays.copyOf(value, value.length));
            }
        } else {
            for (Node[] value : values) {
                boolean in = true;
                int i = 0;
                while (in && i < value.length) {
                    if (item[i] != null)
                        in = item[i].equals(value[i]);
                    i++;
                }
                if (in)
                    ret.add(Arrays.copyOf(value, value.length));
            }
        }
        return ret;
    }

    public boolean contains(Node[] item) {
        for (Node[] value : values) {
            if (Arrays.equals(value, item))
                return true;
        }
        return false;
    }
}
