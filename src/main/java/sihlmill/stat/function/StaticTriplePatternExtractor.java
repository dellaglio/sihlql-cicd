/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stat.function;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.reasoner.TriplePattern;
import org.apache.jena.riot.system.StreamRDFBase;

import java.util.Set;

public class StaticTriplePatternExtractor extends StreamRDFBase {
    private int varPos;
    private Set<Node> bindings;
    private TriplePattern tp;

    public StaticTriplePatternExtractor(Set<Node> bindings, int varPos, TriplePattern tp) {
        this.varPos = varPos;
        this.bindings = bindings;
        this.tp = tp;
    }

    @Override
    public void triple(Triple triple) {
        if (tp.compatibleWith(new TriplePattern(triple))) {
            switch (varPos) {
                case 0:
                    bindings.add(triple.getSubject());
                    break;
                case 1:
                    bindings.add(triple.getPredicate());
                    break;
                case 2:
                    bindings.add(triple.getObject());
                    break;
                default:
                    throw new RuntimeException("Not implemented");
            }
        }
    }
}
