/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.io.kafka;

import com.fasterxml.jackson.core.JsonParseException;
import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.core.RDFDataset;
import com.github.jsonldjava.core.RDFDataset.Quad;
import com.github.jsonldjava.utils.JsonUtils;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.graph.GraphFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

public class JSONSerializer {
    private static final Logger logger = LoggerFactory.getLogger(JSONSerializer.class);

    public static TimestampedGraph deserializeGraph(String jsonString) {
        // logger.debug("Input {}", jsonString);

        RDFDataset dataset;
        try {
            // try to parse the input stream with json ld java
            dataset = (RDFDataset) JsonLdProcessor.toRDF(JsonUtils.fromString(jsonString));
            // logger.debug("Dataset {}", dataset.graphNames());

            // initialize graph name and timestamp
            String graphName = null;
            String graphTimestamp = null;

            // extract the graph name and timestamp from the dataset
            for (Quad q : dataset.getQuads("@default")) {
                if (q.getPredicate().getValue().equals("http://www.w3.org/ns/prov#generatedAtTime")) {
                    // logger.debug("Q {}", q.toString());

                    graphName = q.getSubject().getValue();
                    graphTimestamp = q.getObject().getValue();
                }
            }

            // compute a result timestamp from the date string
            Long resultTimestamp = DatatypeConverter.parseDateTime(graphTimestamp).getTimeInMillis();
            // logger.debug("Name {} - Timestamp {} {}", graphName, graphTimestamp, resultTimestamp);

            // convert all parts of the dataset to timestamped graphs
            if (graphName != null) {
                Graph g = GraphFactory.createGraphMem();

                for (Quad q : dataset.getQuads(graphName)) {
                    // create a jena graph from the quad
                    g.add(Triple.create(NodeFactory.createURI(q.getSubject().getValue()),
                            NodeFactory.createURI(q.getPredicate().getValue()), NodeFactory.createURI(q.getObject().getValue())));

                }

                // convert the jena graph to a timestamped graph
                TimestampedGraph tGraph = new TimestampedGraph(g, resultTimestamp);
                // logger.debug("t@{} tGraph {}", tGraph.getTimestamp(), tGraph.getGraph());

                // add the timestamped graph to the result list
                return tGraph;
            }
        } catch (JsonParseException | JsonLdError e) {
            logger.error(e.getMessage());
        } catch (IllegalArgumentException | IOException e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    public static String serializeGraph(TimestampedGraph graph) {
        logger.debug("Input {}", graph);

        return null;
    }

    //builds a stream of bindings as in SELECT JSON output recommendation
    public static String serializeSelectOutput(long timestamp, Iterable<? extends Tuple> tuples, Map<Var, Integer> vars) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"head\": { \"vars\": [ ");

        StringBuilder head = new StringBuilder();
        int totVar = 0;
        for (Entry<Var, Integer> e : vars.entrySet())
            if (e.getValue() > 0) {
                head.append("\"" + e.getKey().toString().substring(1) + "\", ");
                totVar++;
            }

        String h = head.toString();
        sb.append(h, 0, h.length() - 2);

        sb.append(" ]");
        sb.append("}, ");
        sb.append("\"timestamp\":\"" + timestamp + "\", ");
        sb.append("\"results\": {");
        sb.append("\"bindings\": [");
        StringBuilder tup = new StringBuilder();
        for (Tuple tuple : tuples) {
            tup.append("{");
            StringBuilder bindings = new StringBuilder();
            int t = 0;
            for (Entry<Var, Integer> e : vars.entrySet())
                if (e.getValue() > 0) {
                    Node n = tuple.getField(e.getValue());
                    bindings.append("\"" + e.getKey().toString().substring(1) + "\": { \"type\": \"");
                    if (n.isURI()) bindings.append("uri");
                    else if (n.isLiteral()) bindings.append("literal");
                    bindings.append("\", \"value\": \"");
                    if (n.isURI()) bindings.append(n.getURI());
                    else if (n.isLiteral()) bindings.append(n.getLiteralValue());
                    bindings.append("\" } ,");
                }
            tup.append(bindings.toString(), 0, bindings.toString().length() - 2);
            tup.append("}, ");
        }
        sb.append(tup.toString(), 0, tup.toString().length() - 2);
        sb.append("]");
        sb.append("}");
        sb.append("}");
        return sb.toString();
    }

    //builds a stream of graphs in JSON-LD
    public static String serializeConstructOutput(Tuple tuple, Map<Var, Integer> vars) {
        StringBuilder sb = new StringBuilder();
        return sb.toString();
    }
}
