/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.io.kafka;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class DataProducer {
    public static void main(String[] args) throws IOException {
        final Logger logger = LoggerFactory.getLogger("debug");

        // create producer properties
        Properties props = new Properties();

        // where is kafka located?
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        // what types of values are we sending? how to convert to bytes?
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // ensure that records are sent instantly
        props.setProperty(ProducerConfig.LINGER_MS_CONFIG, "1");
        // props.setProperty(ProducerConfig.ACKS_CONFIG, "all");

        // create producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);

        // read all lines of the json example file
        List<String> jsonData = Files.readAllLines(Paths.get("src/main/resources/json_inputs"));
        for (String jsonGraph : jsonData) {
            // sleep to slow down the loop and simulate distributed arrival of data
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                logger.error("Thread interrupted...");
            }

            // create a record to be sent to the topic
            // TODO: extract the person id and key by this (log-compaction would keep the last action of a user?)
            ProducerRecord<String, String> record = new ProducerRecord<String, String>("psql", jsonGraph);
            logger.debug("Processing {} {}", record);

            // send data (asynchronously)
            producer.send(record, new Callback() {
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    // execute everytime a record is sent or an exception is thrown
                    if (e == null) {
                        logger.debug("Generated record for {}", jsonGraph);
                    } else {
                        logger.error(e.getMessage());
                    }
                }
            });
        }

        producer.flush();
        producer.close();
    }
}
