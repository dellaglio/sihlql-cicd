/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.io.kafka;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Properties;

public class DataSinkBuilder {
    public static FlinkKafkaProducer<String> createKafkaSink(String endpoint, String topic) {
        // create producer properties
        Properties props = new Properties();

        // where is kafka located?
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, endpoint);

        // what types of values are we sending? how to convert to bytes?
        // props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        // props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // ensure that records are sent instantly
        props.setProperty(ProducerConfig.LINGER_MS_CONFIG, "1");

        return new FlinkKafkaProducer<String>(topic, new SimpleStringSchema(), props);
    }
}
