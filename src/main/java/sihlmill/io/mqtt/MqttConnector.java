package sihlmill.io.mqtt;

import org.apache.flink.streaming.api.functions.source.SourceFunction;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

public class MqttConnector implements SourceFunction<String> {
	private MqttClient client;
	private String topic;
	public MqttConnector(String url, String topic) {
		try {
			client = new MqttClient(url, "client"+System.currentTimeMillis());
			client.connect();
			
		} catch (MqttSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.topic = topic;
	}

	@Override
	public void run(SourceContext<String> ctx) throws Exception {
		client.setCallback(new MqttCallback() {
			
			@Override
			public void messageArrived(String topic, MqttMessage message) throws Exception {
				System.out.println(message);
				ctx.collect(message.toString());
			}
			
			@Override
			public void deliveryComplete(IMqttDeliveryToken token) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void connectionLost(Throwable cause) {
				System.err.println("Connection lost");
				cause.printStackTrace();
			}
		});
		client.subscribe(topic);
	
		
	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		
	}
}