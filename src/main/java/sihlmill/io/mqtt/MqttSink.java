package sihlmill.io.mqtt;

import org.apache.flink.streaming.api.functions.sink.SinkFunction;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttSink implements SinkFunction<String> {
	private MqttClient broker;
	private String topic;
	
	public MqttSink(String mqttBrokerUrl, String topic){
		this.topic = topic;
		MemoryPersistence pers = new MemoryPersistence();
		try {
			broker = new MqttClient(mqttBrokerUrl, topic, pers);
			MqttConnectOptions opts = new MqttConnectOptions();
			opts.setCleanSession(true);
			broker.connect(opts);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void invoke(String value, Context context) throws Exception {
		MqttMessage msg = new MqttMessage(value.getBytes());
		try {
			broker.publish(topic, msg);
		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
	
}