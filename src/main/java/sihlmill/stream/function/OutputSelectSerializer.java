/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stream.function;

import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.jena.graph.Node;
import org.apache.jena.sparql.core.Var;

import java.util.Map;
import java.util.Map.Entry;

public class OutputSelectSerializer<T extends Tuple> extends ProcessAllWindowFunction<T, String, TimeWindow> {
    private Map<Var, Integer> vars;

    public OutputSelectSerializer(Map<Var, Integer> vars) {
        this.vars = vars;
    }

    public void process(Context ctx, Iterable<T> winContent, Collector<String> out) throws Exception {
        StringBuilder sb = generateElement(ctx, winContent);

        out.collect(sb.toString());
    }

	protected StringBuilder generateElement(Context ctx, Iterable<T> winContent) {
		StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"head\": { \"vars\": [ ");

        StringBuilder head = new StringBuilder();
        int totVar = 0;
        for (Entry<Var, Integer> e : vars.entrySet())
            if (e.getValue() > 0) {
                head.append("\"" + e.getKey().toString().substring(1) + "\", ");
                totVar++;
            }

        String h = head.toString();
        sb.append(h, 0, h.length() - 2);

        sb.append(" ]");
        sb.append("}, ");
        sb.append("\"timestamp\":\"" + ctx.window().maxTimestamp() + "\", ");
        sb.append("\"results\": {");
        sb.append("\"bindings\": [");
        StringBuilder tup = new StringBuilder();
        for (Tuple tuple : winContent) {
            StringBuilder bindings = new StringBuilder();
            int t = 0;
            for (Entry<Var, Integer> e : vars.entrySet())
                if (e.getValue() > 0) {
                    Node n = tuple.getField(e.getValue());
                    if (n != null) {
                        bindings.append("\"" + e.getKey().toString().substring(1) + "\": { \"type\": \"");
                        if (n.isURI()) bindings.append("uri");
                        else if (n.isLiteral()) bindings.append("literal");
                        bindings.append("\", \"value\": \"");
                        if (n.isURI()) bindings.append(n.getURI());
                        else if (n.isLiteral()) bindings.append(n.getLiteralValue());
                        bindings.append("\" } ,");
                    }
                }
            if (bindings.toString().length() > 2) {
                tup.append("{");
                tup.append(bindings.toString(), 0, bindings.toString().length() - 2);
                tup.append("}, ");
            }
        }
        if (tup.toString().length() > 2)
            sb.append(tup.toString(), 0, tup.toString().length() - 2);
        sb.append("]");
        sb.append("}");
        sb.append("}");
		return sb;
	}
}
