/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stream.function;

import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.writer.JsonLDWriter;
import org.apache.jena.sparql.core.DatasetGraphFactory;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.binding.Binding;
import org.apache.jena.sparql.engine.binding.BindingHashMap;
import org.apache.jena.sparql.graph.GraphFactory;
import org.apache.jena.sparql.modify.TemplateLib;

import java.io.ByteArrayOutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class OutputConstructSerializer<T extends Tuple> extends ProcessAllWindowFunction<T, String, TimeWindow> {
    private Map<Var, Integer> vars;
    private List<Triple> triplePatterns;
    private String streamIri;

    public OutputConstructSerializer(List<Triple> tp, Map<Var, Integer> vars, String streamIri) {
        this.vars = vars;
        triplePatterns = tp;
        this.streamIri = streamIri;
    }

    public void process(Context ctx, Iterable<T> winContent, Collector<String> out) throws Exception {
        String element = generateElement(ctx, winContent);
        out.collect(element);
    }

	protected String generateElement(Context ctx, Iterable<T> winContent) {
		Set<Binding> bindings = new HashSet<>();
        winContent.forEach(tuple -> {
            BindingHashMap b = new BindingHashMap();
            for (Entry<Var, Integer> var : vars.entrySet()) {
                if (var.getValue() > 0)
                    b.add(var.getKey(), tuple.getField(var.getValue()));
            }
            bindings.add(b);
//			TemplateLib.subst(triple, b, bNodeMap)
//			out.collect(JSONSerializer.serializeSelectOutput(ctx.window().maxTimestamp(), winContent, vars));
        });
        Graph graph = GraphFactory.createGraphMem();
        TemplateLib.calcTriples(triplePatterns, bindings.iterator()).forEachRemaining(triple -> graph.add(triple));
        JsonLDWriter writer = new JsonLDWriter(RDFFormat.JSONLD_COMPACT_FLAT);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writer.write(baos, DatasetGraphFactory.createOneGraph(graph), null, null, null);
        String str = baos.toString();
        String graphInfo = ", \"@id\":\""
                + streamIri
                +ctx.window().maxTimestamp()
                + "\", \"http://www.w3.org/ns/prov#generatedAtTime\":\""
                + ctx.window().maxTimestamp()
                + "\"}";
        if(str.length()==3)
            str = "{\"@graph\":[]  ";
        return str.substring(0, str.length() - 2) + graphInfo;
	}
}
