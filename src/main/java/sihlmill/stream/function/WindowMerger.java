/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stream.function;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

public class WindowMerger implements AggregateFunction<Model, Model, Model> {
    @Override
    public Model add(Model g, Model agg) {
        return agg.union(g);
    }

    @Override
    public Model createAccumulator() {
        return ModelFactory.createDefaultModel();
    }

    @Override
    public Model getResult(Model m) {
        return m;
    }

    @Override
    public Model merge(Model g0, Model g1) {
        return g0.union(g1);
    }
}

