package sihlmill.stream.function;

import java.io.StringReader;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Triple;
import org.apache.jena.mem.GraphMem;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.RDFParserBuilder;
import org.apache.jena.riot.system.StreamRDF;
import org.apache.jena.sparql.core.Quad;

public class JsonObjectToTimestampedGraph implements MapFunction<String, TimestampedGraph> {
	@Override
	public TimestampedGraph map(String value) throws Exception {
		TimestampedGraphParser tgp = new TimestampedGraphParser();
		
		RDFParser.fromString(value.toString()).lang(Lang.JSONLD).parse(tgp);
		return tgp.getTimestampedGraph();
	}

	private class TimestampedGraphParser implements StreamRDF {
		private Graph g;
		private long timestamp;
		private TimestampedGraph tg;
		boolean end = false;

		@Override
		public void triple(Triple triple) {
			if(triple.getPredicate().toString().equals("http://www.w3.org/ns/prov#generatedAtTime"))
				timestamp = Long.parseLong((String) triple.getObject().getLiteralValue());
		}

		@Override
		public void start() {
			g = new GraphMem();
		}

		@Override
		public void quad(Quad quad) {
			g.add(quad.asTriple());
		}

		@Override
		public void prefix(String prefix, String iri) {
		}

		@Override
		public void finish() {
			tg = new TimestampedGraph(g, timestamp);
			end = true;
		}

		@Override
		public void base(String base) {
		}
		
		public TimestampedGraph getTimestampedGraph() {
			if(end)
				return tg;
			return null;
		}
	}
}
