/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stream.function;

import org.apache.flink.api.common.functions.MapFunction;

public class OutputFileSerializer implements MapFunction<String, String> {
	private boolean first = true;
	
    public OutputFileSerializer() {
    	super();
    }

	@Override
	public String map(String value) throws Exception {
		if(first) { 
			first = false; 
			return "[\n" + value; 
		} else { 
			return ",\n" + value; 
		}
	}
}
