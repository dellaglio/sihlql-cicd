/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stream.function;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.graph.GraphFactory;

import sihlmill.data.TimestampedModel;
import sihlmill.data.time.SingleTimeAnnotation;

public class TimestampedTripleToModel implements MapFunction<Tuple4<Long, Node, Node, Node>, Model> {
    @Override
    public Model map(Tuple4<Long, Node, Node, Node> tuple) throws Exception {
        Graph g = GraphFactory.createDefaultGraph();
        g.add(Triple.create(tuple.f1, tuple.f2, tuple.f3));
        return new TimestampedModel(new SingleTimeAnnotation(tuple.f0), g);
    }
}
