/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stream.function;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.util.Collector;
import org.apache.jena.graph.Node;
import org.apache.jena.sparql.core.Var;

import sihlmill.stat.function.StaticBindings;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StaticStreamLeftJoinFlatMap<I extends Tuple, O extends Tuple> implements FlatMapFunction<I, O> {
    private StaticBindings staticBindings;
    private Set<Node[]> staticBindingSet;
    private Map<Var, Integer> streamVars;

    public StaticStreamLeftJoinFlatMap(StaticBindings staticBindings, Map<Var, Integer> streamVars) {
        this.staticBindings = staticBindings;
        this.staticBindingSet = staticBindings.retrieve(null);
        this.streamVars = streamVars;
    }

    @Override
    public void flatMap(I value, Collector<O> out) throws Exception {
        List<Var> staticVars = staticBindings.getVars();
        Node[] item = new Node[staticVars.size()];
        int i = 0;
        Set<Var> staticOnlyVars = new HashSet<>();
        Set<Var> commonVars = new HashSet<>();
        for (Var var : staticVars) {
            if (streamVars.containsKey(var)) {
                item[i++] = value.getField(streamVars.get(var));
                commonVars.add(var);
            } else {
                item[i++] = null;
                staticOnlyVars.add(var);
            }
        }

        for (Node[] staticBinding : staticBindingSet) {
            Tuple tmp = Tuple.getTupleClass(streamVars.size() + staticOnlyVars.size()).newInstance();
            tmp.setField(value.getField(0), 0);

            boolean in = true;
            int c = 0;
            while (in && c < staticBinding.length) {
                if (item[c] != null)
                    in = item[c].equals(staticBinding[c]);
                c++;
            }

            if (in) {
                for (Var v : streamVars.keySet()) {
                    i = streamVars.get(v);
                    tmp.setField(value.getField(i), i);
                }
            } else {
                for (Var v : commonVars) {
                    if (streamVars.keySet().contains(v)) {
                        i = streamVars.get(v);
                        tmp.setField(staticBinding[staticVars.indexOf(v)], i);
                    }
                }
            }
            i = streamVars.size();
            for (Var var : staticOnlyVars) {
                tmp.setField(staticBinding[staticVars.indexOf(var)], i++);
            }
            out.collect((O) tmp);
        }
    }
}
