/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stream.function;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.jena.graph.Node;
import org.apache.jena.sparql.core.Var;

import sihlmill.stat.function.StaticBindings;

import java.util.List;
import java.util.Map;

public class StaticStreamJoinFilter<T extends Tuple> implements FilterFunction<T> {
    private StaticBindings staticBindings;
    private Map<Var, Integer> streamVars;

    public StaticStreamJoinFilter(StaticBindings staticBindings, Map<Var, Integer> streamVars) {
        this.staticBindings = staticBindings;
        this.streamVars = streamVars;
    }

    @Override
    public boolean filter(T tuple) throws Exception {
//		System.out.println(tuple);
        List<Var> staticVars = staticBindings.getVars();
        Node[] item = new Node[staticVars.size()];
        int i = 0;
        for (Var var : staticVars) {
            item[i++] = tuple.getField(streamVars.get(var));
        }
        return staticBindings.contains(item);
    }

}
