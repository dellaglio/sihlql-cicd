/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.stream.function;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.util.Collector;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.graph.Triple;
import org.apache.jena.sparql.algebra.Algebra;
import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.core.BasicPattern;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.QueryIterator;
import org.apache.jena.sparql.engine.binding.Binding;

import sihlmill.data.TimestampedModel;
import sihlmill.data.time.SingleTimeAnnotation;

import java.util.List;
import java.util.Map;

public class BGPEvalDisj<T extends Tuple> implements FlatMapFunction<TimestampedGraph, T> {
	private final List<Triple> triplePatterns;
	private final Map<Var, Integer> vars;

	public BGPEvalDisj(List<Triple> triplePatterns, Map<Var, Integer> vars) {
		this.triplePatterns = triplePatterns;
		this.vars = vars;
	}

	@Override
	public void flatMap(TimestampedGraph g, Collector<T> out) throws Exception {
		BasicPattern bp = new BasicPattern();
		triplePatterns.forEach(t -> bp.add(t));
		OpBGP opBgp = new OpBGP(bp);
		final QueryIterator it = Algebra.exec(opBgp, g.getGraph());
		while (it.hasNext()) {
			final Binding b = it.nextBinding();
			final Tuple t = Tuple.getTupleClass(vars.size()).newInstance();

			vars.entrySet().forEach(e -> t.setField(b.get(e.getKey()), e.getValue()));
			t.setField(new SingleTimeAnnotation(g.getTimestamp()), 0);
			out.collect((T) t);
		}
	}
}
