/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.incremental.small.wevent.function;

import org.apache.commons.math3.distribution.LaplaceDistribution;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.util.Collector;
import org.apache.jena.graph.Node;

import sihlmill.data.time.SingleTimeAnnotation;
import sihlmill.whole.small.wevent.function.HistogramFixedBins;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class IncrementalHistogramFixedBins<I extends Tuple, O extends Tuple> extends HistogramFixedBins<I, O> {
    protected List<Double[]> gt;

    public IncrementalHistogramFixedBins(int w, Set<Node> keys, double epsilon, Class<O> returnClass, int inputField) {
        super(w, keys, epsilon, returnClass, inputField);
        gt = new ArrayList<>();
    }

    protected void step7(Context ctx, Collector<O> out, Double[] c_i, double epsilon_rm, double lambda_i2)
            throws InstantiationException, IllegalAccessException {
        int i;
        LaplaceDistribution lap = new LaplaceDistribution(rg, 0, lambda_i2);
        for (i = 0; i < d; i++) {
            c_i[i] += lap.sample();
            O ret = returnClass.newInstance();
            ret.setField(new SingleTimeAnnotation(ctx.window().getEnd() - 1), 0);
            for (int index = 1; index < ret.getArity() - 2; index++) {
                if (index == inputField) {
                    ret.setField(values.get(i), index);
                } else
                    ret.setField(null, index);

            }
            ret.setField(c_i[i], ret.getArity() - 2);
            ret.setField(gt.get(0)[i], ret.getArity() - 1);
            out.collect(ret);
        }
    }

    protected Double[] step1(Iterable<I> in) {
        Double[] c_i;
        if (gt.size() == 0) {
            c_i = new Double[d];
            Arrays.fill(c_i, 0d);
        } else {
            c_i = Arrays.copyOf(gt.get(0), d);
        }
        in.forEach(tuple -> {
            if (keys.containsKey(tuple.getField(inputField).hashCode())) {
                c_i[keys.get(tuple.getField(inputField).hashCode())] += (double) tuple.getField(tuple.getArity() - 1);
            }
        });
        return c_i;
    }

    protected void saveState(Double[] c_i) {
        gt.add(0, Arrays.copyOf(c_i, c_i.length));
    }
}
