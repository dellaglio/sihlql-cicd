/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.incremental.small.wevent.function;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.LaplaceDistribution;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import sihlmill.data.time.SingleTimeAnnotation;
import sihlmill.whole.small.wevent.function.HistogramVariableBins;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class IncrementalHistogramVariableBins<I extends Tuple, O extends Tuple> extends HistogramVariableBins<I, O> {
    private Map<Integer, Double> gt;

    public IncrementalHistogramVariableBins(int w, double epsilon, double h0, double z, double[] epsWeights, Class<O> returnClass, int inputField) {
        super(w, epsilon, h0, z, epsWeights, returnClass, inputField);
        gt = new HashMap<>();
    }

    @Override
    protected void step7(Context ctx, Collector<O> out, Map<Integer, Double> c_i, double epsilon_rm, double lambda_i2)
            throws InstantiationException, IllegalAccessException {
        int index;
        LaplaceDistribution lap = new LaplaceDistribution(0, lambda_i2);
        for (Entry<Integer, Double> entry : c_i.entrySet()) {
            ois.put(entry.getKey(), entry.getValue());
            gt.put(entry.getKey(), c_i.get(entry.getKey()));
            c_i.put(entry.getKey(), entry.getValue() + lap.sample());
            O ret = returnClass.newInstance();
            ret.setField(new SingleTimeAnnotation(ctx.window().getEnd() - 1), 0);
            for (index = 1; index < ret.getArity() - 2; index++) {
                if (index == inputField) {
                    ret.setField(keys.get(entry.getKey()), index);
                } else
                    ret.setField(null, index);

            }
            ret.setField(c_i.get(entry.getKey()), ret.getArity() - 2);
            ret.setField(gt.get(entry.getKey()), ret.getArity() - 1);
            out.collect(ret);
        }
    }
    
    @Override
    protected void step1(Iterable<I> in, Map<Integer, Double> c_i) {
        in.forEach(tuple -> {
            int key = tuple.getField(inputField).hashCode();
            if (!keys.containsKey(key)) {
                c_i.put(key, 0d);
                this.keys.put(key, tuple.getField(inputField));
            } else if (!c_i.containsKey(key)) {
                c_i.put(key, gt.get(key));
            }
            c_i.put(key, c_i.get(key) + (double) tuple.getField(tuple.getArity() - 1));
        });
    }

    @Override
    protected void saveState(Map<Integer, Double> c_i) {
        for (Entry<Integer, Double> entry : c_i.entrySet()) {
            gt.put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    protected int stepRemoval(Map<Integer, Double> c_i) {
        for (Entry<Integer, Double> entry : c_i.entrySet()) {
//			gt.put(entry.getKey(), entry.getValue());
            double prob = removalProbability(entry.getValue(), k, h0);
            BinomialDistribution bd = new BinomialDistribution(1, prob);
            if (bd.sample() == 0)
                toBeRemoved.add(entry.getKey());
        }

        for (int key : toBeRemoved) {
            c_i.remove(key);
        }

        int d = c_i.size();
        return d;
    }
}
