/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.incremental.small.function;

import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.jena.graph.Node;

import sihlmill.data.time.SingleTimeAnnotation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CentralisedIncrementalHistogramFixedBins<I extends Tuple, O extends Tuple> extends ProcessAllWindowFunction<I, O, TimeWindow> {
    protected Double[] gt;
	protected int inputField;
	protected Map<Integer, Integer> keys;
	protected List<Double[]> ois;
	protected final Class<O> returnClass;
	protected Map<Integer, Node> values;
	protected int d;

    public CentralisedIncrementalHistogramFixedBins(Set<Node> keys, Class<O> returnClass, int inputField) {
	    this.d = keys.size();

	    gt = new Double[d];
	    this.keys = new HashMap<>(d);
	    this.values = new HashMap<>(d);
	
	    int i = 0;
	    for (Node key : keys) {
	        gt[i] = 0d;
	        this.values.put(i, key);
	        this.keys.put(key.hashCode(), i++);
	    }
	
	    this.returnClass = returnClass;
	    this.inputField = inputField;
	}

	@Override
	public void process(
	        Context ctx,
	        Iterable<I> in,
	        Collector<O> out) throws Exception {
	
	    long currentTime = ctx.window().getEnd() - 1;
		
	    for(Tuple tuple : in) {
	    	gt[keys.get(tuple.getField(inputField).hashCode())] += (Double)tuple.getField(tuple.getArity() - 1); 
	    }
	    
        for (int i = 0; i < d; i++) {
            O ret = returnClass.newInstance();
            ret.setField(new SingleTimeAnnotation(currentTime), 0);

            for (int index = 1; index < ret.getArity() - 2; index++) {
                if (index == inputField) {
                    ret.setField(values.get(i), index);
                } else
                    ret.setField(null, index);

            }
            ret.setField(gt[i], ret.getArity() - 1);
            out.collect(ret);
        }
	}
}
