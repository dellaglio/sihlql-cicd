package sihlmill.whole.big.compiler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.apache.jena.sparql.algebra.op.OpGroup;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.expr.Expr;
import org.apache.jena.sparql.expr.ExprAggregator;
import org.apache.jena.sparql.expr.aggregate.AggCount;
import org.apache.jena.sparql.expr.aggregate.AggCountVar;
import org.apache.jena.sparql.expr.aggregate.AggSum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sihlmill.compiler.rewriter.J2FWithTimeAnnotation;
import sihlmill.compiler.rewriter.Vars;

public class J2F extends J2FWithTimeAnnotation {
	private Logger logger = LoggerFactory.getLogger(J2F.class); 

	protected String execute(OpGroup opGroup, String input, Vars vs, FileWriter writer) {
        Map<Var, Integer> vars = vs.getVars();

        try {
            writer.write("//before executing the GROUP block\n");
        } catch (IOException e) {
            logger.error("Writing error: {}", e);
            throw new RuntimeException(e);
        }
        String subStream = exec(opGroup.getSubOp(), input, vs, writer);

        String dataStreamReturnType = "TimeAnnotation";
        String keyByReturnType = "";
        for (int i = 0; i < vars.size() - 1; i++)
            dataStreamReturnType += ",Node";

        String pre = "\n";
        String post = "";

        int keyByArgs = 0;

        if (opGroup.getAggregators().size() != 1)
            throw new RuntimeException("Not implemented");

        String extendedReturnType = dataStreamReturnType + ",Double";
        int extendedVarCount = vars.size() + 1;

        ExprAggregator ea = opGroup.getAggregators().get(0);
        pre += ".map(new MapFunction<Tuple" + vars.size() + "<" + dataStreamReturnType + ">, Tuple" + (vars.size() + 1) + "<" + extendedReturnType + ">>() {\n" +
                "@Override\n" +
                "public Tuple" + (vars.size() + 1) + "<" + extendedReturnType + "> map(Tuple" + vars.size() + "<" + dataStreamReturnType + "> value) throws Exception {\n" +
                "return new Tuple" + extendedVarCount + "<>(value.f0";

        post += ".reduce((v1,v2) -> new Tuple" + extendedVarCount + "<" + extendedReturnType + ">(new SingleTimeAnnotation(Math.max(v1.f0.getTimestamp(), v2.f0.getTimestamp()))";

        for (int i = 1; i < vars.size(); i++) {
            pre += ", value.f" + i;
            post += ", v1.f" + i;
        }

        if (ea.getAggregator() instanceof AggCount || ea.getAggregator() instanceof AggCountVar) {
            pre += ", 1d);\n" +
                    "}\n" +
                    "})\n";
        } else if (ea.getAggregator() instanceof AggSum) {
            Expr arg = ea.getAggregator().getExprList().get(0);
            if (!arg.isVariable() || ea.getAggregator().getExprList().size() > 1)
                throw new RuntimeException("not implemented");

            pre += ", value.f" + vars.get(arg.asVar()) + "==null?0:((Number)value.f" + vars.get(arg.asVar()) + ".getLiteralValue()).doubleValue());\n" +
                    "}\n" +
                    "})\n";
            //		} else if(){
        } else {
            throw new RuntimeException(ea.getAggregator().getClass() + " not implemented");
        }

        post += ", v1.f" + vars.size() + "+v2.f" + vars.size() + "))\n";

        post += ".map(new MapFunction<Tuple" + extendedVarCount + "<" + extendedReturnType + ">, Tuple" + (vars.size() + 1) + "<" + dataStreamReturnType + ",Node>>(){\n" +
                "@Override\n" +
                "public Tuple" + extendedVarCount + "<" + dataStreamReturnType + ",Node> map(Tuple" + extendedVarCount + "<" + extendedReturnType + "> value) throws Exception {\n" +
                "vars" + vs.getCounter() + ".put(Var.alloc(\".0\"), vars" + vs.getCounter() + ".size());\n" +
                "return new Tuple" + extendedVarCount + "<" + dataStreamReturnType + ",Node>(value.f0";
        for (int i = 1; i < vars.size(); i++)
            post += ", value.f" + i;
        post += ", NodeFactory.createLiteralByValue(value.f" + vars.size() + ", XSDDatatype.XSDdouble));\n" +
                "}\n" +
                "\n" +
                "})\n";

        vars.put(Var.alloc(ea.getAggVar().asVar().getName()), vars.size());
        keyByReturnType = dataStreamReturnType + ",Double";
        dataStreamReturnType += ",Node";
        keyByArgs = vars.size();


        long id = System.nanoTime();
        String ret = "ds" + id;

        try {
            writer.write("//begin of the GROUP block\n");
            writer.write("DataStream<Tuple" + vars.size() + "<" + dataStreamReturnType + ">> " + ret + " = " + subStream + pre
                    + ".assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Tuple" + keyByArgs + "<" + keyByReturnType + ">>(){\n"
                    + "@Override\n"
                    + "public long extractAscendingTimestamp(Tuple" + vars.size() + "<" + keyByReturnType + "> element) {\n"
                    + "return element.f0.getTimestamp();\n"
                    + "}\n"
                    + "})\n"
                    + ".keyBy(");
            writer.write("new KeySelector<Tuple" + keyByArgs + "<" + keyByReturnType + ">, Integer>(){\n");
            writer.write("@Override\n");
            writer.write("public Integer getKey(Tuple" + keyByArgs + "<" + keyByReturnType + "> value) throws Exception {\n");
            writer.write("return ");
//			String v = "";
            for (Var tmp : opGroup.getGroupVars().getVars()) {
//				v += vars.get(tmp) + ",";
                writer.write("(value.getField(" + vars.get(tmp) + ")==null?0:value.getField(" + vars.get(tmp) + ").hashCode())+");
            }
            writer.write("0;\n");
//			writer.write("return value.getField("+v.substring(0, v.length()-1)+").hashCode();\n");
            writer.write("}\n");
            writer.write("})\n");
            writer.write(".timeWindow(Time.milliseconds(1))\n");
            writer.write(post);
            writer.write(";\n");

            //			vars.put(Var.alloc(ea.getVarName()), vars.size());
            createNewVars(vs, writer);
            writer.write("//end of the GROUP block\n");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //			qIter = new QueryIterGroup(qIter, opGroup.getGroupVars(), opGroup.getAggregators(), execCxt) ;

        return ret;
    }
}
