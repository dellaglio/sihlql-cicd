package sihlmill.whole.compiler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.op.Op0;
import org.apache.jena.sparql.algebra.op.Op1;
import org.apache.jena.sparql.algebra.op.Op2;
import org.apache.jena.sparql.algebra.op.OpGraph;
import org.apache.jena.sparql.algebra.op.OpGroup;
import org.apache.jena.sparql.algebra.op.OpJoinStaticStreamFilter;
import org.apache.jena.sparql.algebra.op.OpLeftJoinStaticStreamFlatMap;
import org.apache.jena.sparql.algebra.op.OpStaticGraphMaterialised;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.expr.Expr;
import org.apache.jena.sparql.expr.ExprAggregator;
import org.apache.jena.sparql.expr.aggregate.AggCount;
import org.apache.jena.sparql.expr.aggregate.AggCountVar;
import org.apache.jena.sparql.expr.aggregate.AggSum;

import sihlmill.compiler.rewriter.J2FWithTimeAnnotation;
import sihlmill.compiler.rewriter.Vars;

public abstract class J2FWholeBase extends J2FWithTimeAnnotation {

	public J2FWholeBase() {
		super();
	}

	@Override
	protected String execute(OpGroup opGroup, String input, Vars vs, FileWriter writer) {
	    Map<Var, Integer> vars = vs.getVars();
	
	    String subStream = "";
	    String staticBindings = "";
	
	
	    if (opGroup.getSubOp() instanceof OpLeftJoinStaticStreamFlatMap) {
	        OpGraph osg = (OpGraph) ((Op2) opGroup.getSubOp()).getLeft();
	        staticBindings = exec(osg, input, vs, writer);
	        Op ojssf = OpJoinStaticStreamFilter.create(new OpStaticGraphMaterialised(osg.getNode(), osg.getSubOp(), staticBindings), ((Op2) opGroup.getSubOp()).getRight());
	        subStream = exec(ojssf, input, vs, writer);
	    } else if (opGroup.getSubOp() instanceof Op1 || opGroup.getSubOp() instanceof Op0) {
	        subStream = exec(opGroup.getSubOp(), input, vs, writer);
	        //TODO: it should end in this branch also when the Op is OpJoinStaticStreamFlatMap...
	    } else
	        throw new RuntimeException("Not implemented for " + opGroup.getSubOp().getClass() + "!");
	
	
	    String dataStreamReturnType = "TimeAnnotation";
	    for (int i = 0; i < vars.size() - 1; i++)
	        dataStreamReturnType += ",Node";
	
	    String pre = "\n";
	    String post = "";
	
	    if (opGroup.getGroupVars().size() != 1)
	        throw new RuntimeException("Not implemented");
	    if (opGroup.getAggregators().size() != 1)
	        throw new RuntimeException("Not implemented");
	
	    String extendedReturnType = dataStreamReturnType + ",Double";
	    int extendedVarCount = vars.size() + 1;
	
	    ExprAggregator ea = opGroup.getAggregators().get(0);
	    pre += ".map(new MapFunction<Tuple" + vars.size() + "<" + dataStreamReturnType + ">, Tuple" + extendedVarCount + "<" + extendedReturnType + ">>() {\n" +
	            "@Override\n" +
	            "public Tuple" + (vars.size() + 1) + "<" + extendedReturnType + "> map(Tuple" + vars.size() + "<" + dataStreamReturnType + "> value) throws Exception {\n" +
	            "return new Tuple" + extendedVarCount + "<>(value.f0";
	
	    for (int i = 1; i < vars.size(); i++) {
	        pre += ", value.f" + i;
	        //			post+=", v1.f"+i;
	    }
	
	    if (ea.getAggregator() instanceof AggCount || ea.getAggregator() instanceof AggCountVar) {
	        pre += ", 1d);\n" +
	                "}\n" +
	                "})\n";
	    } else if (ea.getAggregator() instanceof AggSum) {
	        Expr arg = ea.getAggregator().getExprList().get(0);
	        if (!arg.isVariable() || ea.getAggregator().getExprList().size() > 1)
	            throw new RuntimeException("not implemented");
	
	        pre += "," + getAggregationVariable(vars.size() - 1) + ");\n"+
	                "}\n" +
	                "})\n";
	    } else {
	        throw new RuntimeException(ea.getAggregator().getClass() + " not implemented");
	    }
	
	    int finalMapVarCount = extendedVarCount + 1;
	    String finalMapInput = extendedReturnType + ",Double";
	    String finalMapOutput = dataStreamReturnType + ",Node,Node";
	    post += ".map(new MapFunction<Tuple" + finalMapVarCount + "<" + finalMapInput + ">, Tuple" + finalMapVarCount + "<" + finalMapOutput + ">>(){\n" +
	            "@Override\n" +
	            "public Tuple" + finalMapVarCount + "<" + finalMapOutput + "> map(Tuple" + finalMapVarCount + "<" + finalMapInput + "> value) throws Exception {\n" +
	            //				"vars"+vs.getCounter()+".put(Var.alloc(\".0\"), vars"+vs.getCounter()+".size());\n" +
	            "return new Tuple" + finalMapVarCount + "<" + finalMapOutput + ">( \nvalue.f0";
	    for (int i = 1; i < vars.size(); i++)
	        post += ", value.f" + i;
	    post += ",\n value.f" + vars.size() + "==null? null : NodeFactory.createLiteralByValue(value.f" + vars.size() + ", XSDDatatype.XSDdouble)"
	            + ",\n value.f" + (vars.size() + 1) + "==null? null : NodeFactory.createLiteralByValue(value.f" + (vars.size() + 1) + ", XSDDatatype.XSDdouble)"
	            + ");\n" +
	            "}\n" +
	            "\n" +
	            "})\n";
	
	    vars.put(Var.alloc(ea.getAggVar().asVar().getName()), vars.size());
	    vars.put(Var.alloc(".gt"), vars.size());
	
	    long id = System.nanoTime();
	    String ret = "ds" + id;
	
	    try {
	        writer.write("//begin of the GROUP block (DP)\n");
	        writer.write("DataStream<Tuple" + finalMapVarCount + "<" + finalMapOutput + ">> " + ret + " = " + subStream + pre);
	        writer.write(".assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Tuple" + extendedVarCount + "<" + extendedReturnType + ">>(){\n"
	                + "@Override\n"
	                + "public long extractAscendingTimestamp(Tuple" + extendedVarCount + "<" + extendedReturnType + "> element) {\n"
	                + "return element.f0.getTimestamp();\n"
	                + "}\n"
	                + "})\n");
	        writer.write(".timeWindowAll(Time.milliseconds(1))\n");
	        if (opGroup.getSubOp() instanceof OpLeftJoinStaticStreamFlatMap) {
	            fixedBinsBlock(opGroup, writer, vars, staticBindings, extendedReturnType, extendedVarCount);
	        } else if (opGroup.getSubOp() instanceof Op1 || opGroup.getSubOp() instanceof Op0) {
	            variableBinsBlock(opGroup, writer, vars, extendedReturnType, extendedVarCount);
	        }
	        writer.write(post);
	        writer.write(";\n");
	        writer.write("//end of the GROUP block (DP)\n");
	
	        //			vars.put(Var.alloc(ea.getVarName()), vars.size());
	        createNewVars(vs, writer);
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	
	    //			qIter = new QueryIterGroup(qIter, opGroup.getGroupVars(), opGroup.getAggregators(), execCxt) ;
	
	    return ret;
	}
	
    protected String getAggregationVariable(int size) {
    	return "1.0";
    }

	protected abstract void variableBinsBlock(OpGroup opGroup, FileWriter writer, Map<Var, Integer> vars,
			String extendedReturnType, int extendedVarCount) throws IOException;

	protected abstract void fixedBinsBlock(OpGroup opGroup, FileWriter writer, Map<Var, Integer> vars,
			String staticBindings, String extendedReturnType, int extendedVarCount) throws IOException;

}