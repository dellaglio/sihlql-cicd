/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.whole.small.function;

import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.jena.graph.Node;

import sihlmill.data.time.SingleTimeAnnotation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class HistogramVariableBins<I extends Tuple, O extends Tuple> extends ProcessAllWindowFunction<I, O, TimeWindow> {
	protected int inputField;
	protected Map<Integer, Node> keys;
	protected final Class<O> returnClass;

    public HistogramVariableBins(Class<O> returnClass, int inputField) {
	    this.keys = new HashMap<>();
	
	    this.returnClass = returnClass;
	    this.inputField = inputField;
	}

	@Override
	public void process(
	        Context ctx,
	        Iterable<I> in,
	        Collector<O> out) throws Exception {
		
        Map<Integer, Double> c_i = new HashMap<>();
        in.forEach(tuple -> {
            int key = tuple.getField(inputField).hashCode();
            if (!keys.containsKey(key)) {
                this.keys.put(key, tuple.getField(inputField));
            }
            if (c_i.containsKey(key)) {
                c_i.put(key, c_i.get(key) + 1);
            } else {
                c_i.put(key, 1d);
            }
        });
        
        for (Entry<Integer, Double> entry : c_i.entrySet()) {
            O ret = returnClass.newInstance();
            ret.setField(c_i.get(entry.getKey()), ret.getArity() - 1);
            
            ret.setField(new SingleTimeAnnotation(ctx.window().getEnd() - 1), 0);
            for (int index = 1; index < ret.getArity() - 2; index++) {
                if (index == inputField) {
                    ret.setField(keys.get(entry.getKey()), index);
                } else
                    ret.setField(null, index);

            }
            out.collect(ret);
        }

	}
}
