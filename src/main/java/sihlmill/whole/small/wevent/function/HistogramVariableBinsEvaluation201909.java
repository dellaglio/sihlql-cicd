/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.whole.small.wevent.function;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.LaplaceDistribution;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.util.Collector;

import sihlmill.data.time.SingleTimeAnnotation;
import sihlmill.wevent.function.HistogramVariableBinsAbstract;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class HistogramVariableBinsEvaluation201909<I extends Tuple, O extends Tuple> extends HistogramVariableBinsAbstract<I, O> {
    public HistogramVariableBinsEvaluation201909(int w, double epsilon, double h0, double z, double[] epsWeights, Class<O> returnClass, int inputField) {
        super(w, epsilon, h0, z, epsWeights, returnClass, inputField);
    }

    @Override
    protected void step8(Context ctx, Collector<O> out, Map<Integer, Double> c_i) throws InstantiationException, IllegalAccessException {
        O ret = returnClass.newInstance();
        ret.setField(new SingleTimeAnnotation(ctx.window().getEnd() - 1), 0);
        out.collect(ret);
    }

    @Override
    protected void step7(Context ctx, Collector<O> out, Map<Integer, Double> c_i, double epsilon_rm, double lambda_i2)
            throws InstantiationException, IllegalAccessException {
        int index;
        double noise = new LaplaceDistribution(rg, 0, lambda_i2).sample();
        for (Entry<Integer, Double> entry : c_i.entrySet()) {
            ois.put(entry.getKey(), entry.getValue()+noise);

            O ret = returnClass.newInstance();
            ret.setField(c_i.get(entry.getKey()), ret.getArity() - 1);
            c_i.put(entry.getKey(), entry.getValue() + noise);
            ret.setField(new SingleTimeAnnotation(ctx.window().getEnd() - 1), 0);
            for (index = 1; index < ret.getArity() - 2; index++) {
                if (index == inputField) {
                    ret.setField(keys.get(entry.getKey()), index);
                } else
                    ret.setField(null, index);

            }
            ret.setField(c_i.get(entry.getKey()), index++);
            Map<String,Object> track = new HashMap<String, Object>();
            track.put("k", k);
            track.put("h0", h0);
            ret.setField(track, index++);
//            System.out.println(ret);
            out.collect(ret);
        }
    }

    @Override
    protected double step3(Map<Integer, Double> c_i, int d, Map<Integer, Double> o_l) {
        double dis = 0d;
        for (Integer key : c_i.keySet()) {
            double o = 0d;
            if (o_l.containsKey(key))
                o = o_l.get(key);
            dis += Math.abs(o - c_i.get(key));
        }
        dis /= d;
        return dis;
    }

    @Override
    protected int stepRemoval(Map<Integer, Double> c_i) {
        for (Entry<Integer, Double> entry : c_i.entrySet()) {
            double prob = removalProbability(entry.getValue(), k, h0);
            BinomialDistribution bd = new BinomialDistribution(rg, 1, prob);
            int b = bd.sample();
//            System.out.println(b + " " + prob + " " + k + " " + h0);
            if (b == 0)
                toBeRemoved.add(entry.getKey());
        }

        for (int key : toBeRemoved) {
            c_i.remove(key);
        }

        int d = c_i.size();
        return d;
    }
}
