/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.whole.small.wevent.compiler;

import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.Var;

import sihlmill.whole.compiler.J2FWholeBase;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

public class J2F extends J2FWholeBase {
    protected double epsilon;
    protected int w;
    protected double h0;

    public J2F(double epsilon, int w, double h0) {
        this.epsilon = epsilon;
        this.w = w;
        this.h0 = h0;
    }

    @Override
    public Set<String> getSpecificPackages() {
    	Set<String> ret = super.getSpecificPackages();
    	ret.add("sihlmill.whole.small.wevent.function.*");
    	return ret;
    }

    protected void variableBinsBlock(OpGroup opGroup, FileWriter writer, Map<Var, Integer> vars,
                                     String extendedReturnType, int extendedVarCount) throws IOException {
        writer.write(".process(new HistogramVariableBins<Tuple" + extendedVarCount + "<" + extendedReturnType + ">,Tuple" + (extendedVarCount + 1) + "<" + extendedReturnType + ",Double>>(\n"
                + w + ", \n"
                + epsilon + ", \n"
                + h0 + ", \n"
                + "Double.NaN, \n"
                + "new double[] {1.0/3, 1.0/3, 1.0/3}, \n"
                + "(Class<Tuple" + (extendedVarCount + 1) + "<" + extendedReturnType + ",Double>>)new Tuple" + (extendedVarCount + 1) + "<" + extendedReturnType + ",Double>().getClass(),"
                + vars.get(opGroup.getGroupVars().getVars().get(0))
                + "),\n"
                + "TypeInformation.of(new TypeHint<Tuple" + (extendedVarCount + 1) + "<" + extendedReturnType + ",Double>>(){}))\n");
    } 

    protected void fixedBinsBlock(OpGroup opGroup, FileWriter writer, Map<Var, Integer> vars, String staticBindings,
                                  String extendedReturnType, int extendedVarCount) throws IOException {
        writer.write(".process(new HistogramFixedBins<Tuple" + extendedVarCount + "<" + extendedReturnType + ">,Tuple" + (extendedVarCount + 1) + "<" + extendedReturnType + ",Double>>(\n"
                + w + ", \n"
                + staticBindings
                + ".getProjection(Var.alloc(\"" + opGroup.getGroupVars().getVars().get(0).getName() + "\")), \n"
                + epsilon + ", \n"
                + "(Class<Tuple" + (extendedVarCount + 1) + "<" + extendedReturnType + ",Double>>)new Tuple" + (extendedVarCount + 1) + "<" + extendedReturnType + ",Double>().getClass(),"
                + vars.get(opGroup.getGroupVars().getVars().get(0))
                + "),\n"
                + "TypeInformation.of(new TypeHint<Tuple" + (extendedVarCount + 1) + "<" + extendedReturnType + ",Double>>(){}))\n");
    }
   
}
