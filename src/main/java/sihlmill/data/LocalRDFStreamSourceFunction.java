/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.data;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.jsfr.json.JsonPathListener;
import org.jsfr.json.JsonSurfer;
import org.jsfr.json.JsonSurferJackson;
import org.jsfr.json.ParsingContext;

public class LocalRDFStreamSourceFunction implements SourceFunction<String> {
	boolean running = true;
	private String streamUri;
	
	public LocalRDFStreamSourceFunction(String uri) {
		streamUri = uri;
	}

	@Override
	public void cancel() {
		running = false;		
	}

	@Override
	public void run(SourceContext<String> context) throws Exception {
	    JsonSurfer surfer = JsonSurferJackson.INSTANCE;
	    surfer.configBuilder().bind("$[*]", new JsonPathListener() {
	        @Override
	        public void onValue(Object value, ParsingContext ctx) {
	        	context.collect(value.toString());
	        }
	    }).buildAndSurf(new FileInputStream(new File(new URI(streamUri))));
	}
}
