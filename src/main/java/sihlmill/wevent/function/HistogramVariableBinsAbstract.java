/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.wevent.function;

import org.apache.commons.math3.distribution.LaplaceDistribution;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.jena.graph.Node;

import java.util.*;

public abstract class HistogramVariableBinsAbstract<I extends Tuple, O extends Tuple> extends ProcessAllWindowFunction<I, O, TimeWindow> {
    protected final Class<O> returnClass;
    protected int w = 3;
    protected double epsilon, lambda_i1;
    protected double h0, z;
    protected Map<Integer, Double> ois;
    protected List<Double> epsilons;
    protected Map<Integer, Node> keys;

    protected int inputField;
    protected Set<Integer> toBeRemoved;
    
    protected double[] epsWeights;
    
    protected RandomGenerator rg = new MersenneTwister(System.nanoTime());

    public HistogramVariableBinsAbstract(int w, double epsilon, double h0, double z, double[] epsWeights, Class<O> returnClass, int inputField) {
        this.w = w;
        if(!Double.isNaN(h0)) {
        	this.h0 = h0;
        	this.z = Double.NaN;
        } else {
        	this.z = z;
        }
        
        this.epsWeights = epsWeights;

        this.keys = new HashMap<>();

        ois = new HashMap<>();

        this.epsilon = epsilon;

        epsilons = new ArrayList<>(w);
        java.util.Collections.fill(epsilons, 0d);

        this.returnClass = returnClass;
        this.inputField = inputField;
    }
    
    protected double epsRem, epsBR1, epsBR2;
    protected double k;

    @Override
    public void process(
            Context ctx,
            Iterable<I> in,
            Collector<O> out) throws Exception {
        long currentTime = ctx.window().getEnd() - 1;

        //step 1
        Map<Integer, Double> c_i = new HashMap<>();
        step1(in, c_i);

        //step 1.5: decide which c_i are candidates for output
        epsRem = epsilon / (w/epsWeights[0]);
    	k = epsRem;

    	if(!Double.isNaN(z)) {
    		h0 = 1/epsRem * Math.log((1-z)/z);
    	}

        //step 2
		toBeRemoved = new HashSet<>();
        int d = stepRemoval(c_i);

//        Map<Integer, Double> o_l = null;
//        int index = 0;
//        while (o_l == null && index < ois.size()) {
//            o_l = ois.get(index++);
//        }
//        if (o_l == null) {
//            o_l = new HashMap<>(d);
//            for (Integer key : c_i.keySet()) {
//                o_l.put(key, 0d);
//            }
//        }

        //step 3
        double dis = step3(c_i, d, ois);
        lambda_i1 = 1/epsWeights[1] * w / (epsilon * d);

        //step 4
        dis += new LaplaceDistribution(rg, 0, lambda_i1).sample();

        //step 5
        if (epsilons.size() == w)
            epsilons.remove(epsilons.size() - 1);

        double epsilon_rm = 1.0/epsWeights[2] * epsilon;
        for (double eps : epsilons) {
            epsilon_rm -= eps;
        }

        //step 6
        double lambda_i2 = 2 / epsilon_rm;

        saveState(c_i);

        //step 7
        if (d > 0 && dis > lambda_i2) {
            step7(ctx, out, c_i, epsilon_rm, lambda_i2);
//            ois.add(0, c_i);
            epsilons.add(0, epsilon_rm / 2);
        }
        //step 8
        else {
//            ois.add(0, null);
            epsilons.add(0, 0d);
            step8(ctx, out, c_i);
        }
    }

    protected void step1(Iterable<I> in, Map<Integer, Double> c_i) {
        in.forEach(tuple -> {
            int key = tuple.getField(inputField).hashCode();
            if (!keys.containsKey(key)) {
                this.keys.put(key, tuple.getField(inputField));
            }
            if (c_i.containsKey(key)) {
                c_i.put(key, c_i.get(key) + 1);
            } else {
                c_i.put(key, 0d);
            }
        });
    }

    protected abstract void step8(Context ctx, Collector<O> out, Map<Integer, Double> c_i) throws InstantiationException, IllegalAccessException;

    protected abstract void step7(Context ctx, Collector<O> out, Map<Integer, Double> c_i, double epsilon_rm, double lambda_i2) throws InstantiationException, IllegalAccessException;

    protected abstract double step3(Map<Integer, Double> c_i, int d, Map<Integer, Double> o_l);

    protected abstract int stepRemoval(Map<Integer, Double> c_i);

    //for the incremental version
    protected void saveState(Map<Integer, Double> c_i) {
    }

    protected double removalProbability(double value, double k, double h0) {
//    	System.out.println(value);
        return 1.0 / (1.0 + Math.exp(-k * (value - h0)));
    }
    
    public void debug(O ret, int index) {
    	
    }
}
