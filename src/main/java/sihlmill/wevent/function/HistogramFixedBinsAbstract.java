/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.wevent.function;

import org.apache.commons.math3.distribution.LaplaceDistribution;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.jena.graph.Node;

import java.util.*;

public abstract class HistogramFixedBinsAbstract<I extends Tuple, O extends Tuple> extends ProcessAllWindowFunction<I, O, TimeWindow> {
    protected final Class<O> returnClass;
    protected int w = 3;
    protected int d = 3;
    protected double epsilon, lambda_i1;
    protected List<Double[]> ois;
    protected List<Double> epsilons;
    protected Map<Integer, Integer> keys;
    protected Map<Integer, Node> values;

    protected int inputField;

    protected RandomGenerator rg = new MersenneTwister(System.nanoTime());

    public HistogramFixedBinsAbstract(int w, Set<Node> keys, double epsilon, Class<O> returnClass, int inputField) {
        this.w = w;
        this.d = keys.size();

        this.keys = new HashMap<>(d);
        this.values = new HashMap<>(d);

        int i = 0;
        for (Node key : keys) {
            this.values.put(i, key);
            this.keys.put(key.hashCode(), i++);
        }

        ois = new ArrayList<>();

        this.epsilon = epsilon;
        lambda_i1 = 2 * w / (epsilon * d);

        epsilons = new ArrayList<>();
        Collections.fill(epsilons, 0d);

        this.returnClass = returnClass;
        this.inputField = inputField;
    }

    @Override
    public void process(
            Context ctx,
            Iterable<I> in,
            Collector<O> out) throws Exception {

        long currentTime = ctx.window().getEnd() - 1;

        //step 1
        Double[] c_i;
        c_i = step1(in);

        //step 2
        Double[] o_l = null;
        int i = 0;
        while (o_l == null && i < ois.size()) {
            o_l = ois.get(i++);
        }
        if (o_l == null) {
            o_l = new Double[d];
            Arrays.fill(o_l, 0d);
        }

        //step 3
        double dis = 0d;
        for (i = 0; i < d; i++) {
            dis += Math.abs(o_l[i] - c_i[i]);
        }
        dis /= d;

        //step 4
        dis += new LaplaceDistribution(rg, 0, lambda_i1).sample();

        //step 5
        if (epsilons.size() == w)
            epsilons.remove(epsilons.size() - 1);

        double epsilon_rm = epsilon / 2;
        for (double eps : epsilons) {
            epsilon_rm -= eps;
        }

        //step 6
        double lambda_i2 = 2 / epsilon_rm;

        saveState(c_i);
        
        //step 7
        if (dis > lambda_i2) {
            step7(ctx, out, c_i, epsilon_rm, lambda_i2);
            ois.add(0, c_i);
            epsilons.add(0, epsilon_rm / 2);
        }
        //step 8
        else {
            ois.add(0, null);
            epsilons.add(0, 0d);
            step8(ctx, out, c_i);
        }

    }

    protected abstract void step8(Context ctx, Collector<O> out, Double[] c_i) throws InstantiationException, IllegalAccessException;

    protected abstract void step7(Context ctx, Collector<O> out, Double[] c_i, double epsilon_rm, double lambda_i2) throws InstantiationException, IllegalAccessException;

    protected abstract Double[] step1(Iterable<I> in);

    protected void saveState(Double[] c_i) {
    }
}
