/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.compiler;

import org.apache.jena.query.SortCondition;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.OpVisitorBase;
import org.apache.jena.sparql.algebra.OpWalker;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.expr.Expr;

import java.util.*;

public class JenaToFlinkVisitor extends OpVisitorBase {

    // ***
    // DECLARATIONS
    // ***

    List<String> topology = new ArrayList<String>();
    Map<String, Integer> schema = new HashMap<String, Integer>();
    int noVars = 0;
    String sTuple = "";


    // ***
    // CONSTRUCTORS
    // ***

    public JenaToFlinkVisitor() {
    }

    public JenaToFlinkVisitor(Map<String, Integer> vars) {
        updateSchema(vars);
    }


    // ***
    // HELPER FUNCTIONS
    // ***

    private int getIndex(String s) {
        if (schema.get(s) != null) {
            return schema.get(s);
        } else {
            return -1;
        }
    }

    private void updateSchema(Map<String, Integer> vars) {
        // set mapping of variables to indices
        schema = vars;
        // get number of variables
        noVars = schema.size();

        // create string for tuple type
        List<String> aNodes = new ArrayList<String>();
        String sNodes;
        for (int i = 0; i < noVars; i++) {
            aNodes.add("Node");
        }
        sNodes = String.join(", ", aNodes);
        sTuple = "Tuple" + noVars + "<" + sNodes + ">";
    }


    // ***
    // MEMBER FUNCTIONS
    // ***

    public String myOpVisitorWalker(Op op) {
        OpWalker.walk(op, this);

        topology.add("\n.print();");
        topology.add("\n\nenv.execute();");

        //return topology string
        return String.join("", topology);
    }

    public void visit(OpModifier op) {
    }


    // ***
    // OVERRIDES
    // ***

    @Override
    public void visit(OpBGP opBGP) {
        // implemented in factory
    }

    @Override
    public void visit(OpProject opProject) {
        ArrayList<String> list = new ArrayList<String>();
        Map<String, Integer> vars = new HashMap<String, Integer>();
        String s;
        Integer i, j = 0;

        // find variables to project and add their index to the list
        for (Var e : opProject.getVars()) {
            s = e.toString().substring(1);
            i = getIndex(s);
            if (i >= 0) {
                list.add(Integer.toString(i));
                vars.put(s, j);
                j++;
            }
        }
        updateSchema(vars);

        // create string for project()
        s = "\t\n.";
        s = s + "<" + sTuple + ">";
        s = s + "project(";
        s = s + String.join(", ", list);
        s = s + ")";

        topology.add(s);

    }

    @Override
    public void visit(OpFilter opFilter) {
        String s, a, b, op;
        Expr e = opFilter.getExprs().get(0);
        int i;

        // get first and second argument, as well as the operator
        a = e.getFunction().getArg(1).toString();
        b = e.getFunction().getArg(2).toString();
        op = e.getFunction().getOpName();

        // check if arguments are variables; if yes, substitute with tuple field
        //TODO: more types
        i = getIndex(a.substring(1));
        if (i >= 0) {
            a = "Integer.parseInt(t.f" + i + ".getLiteralValue().toString())";
        }

        i = getIndex(b.substring(1));
        if (i >= 0) {
            b = "Integer.parseInt(t.f" + i + ".getLiteralValue().toString())";
        }

        // expand = to ==
        if (op.equals("=")) {
            op = "==";
        }

        // create string for filter()
        s = "\t\n.filter(" +
                "new FilterFunction<" +
                sTuple +
                ">() {\n" +
                "    public boolean filter(" +
                sTuple +
                " t) throws Exception {\n" +
                "        return " +
                a +
                " " +
                op +
                " " +
                b +
                " " +
                ";\n" +
                "    }\n" +
                "}" +
                ")";

        topology.add(s);
    }

    @Override
    public void visit(OpQuadPattern quadPattern) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpQuadBlock quadBlock) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpTriple opTriple) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpQuad opQuad) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpPath opPath) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpProcedure opProc) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpPropFunc opPropFunc) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpJoin opJoin) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpSequence opSequence) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpDisjunction opDisjunction) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpLeftJoin opLeftJoin) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpConditional opCond) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpMinus opMinus) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpDiff opDiff) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpUnion opUnion) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpGraph opGraph) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpService opService) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpDatasetNames dsNames) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpTable opTable) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    // Has a default.
    // @Override public void visit(OpExt opExt) {}

    @Override
    public void visit(OpNull opNull) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpLabel opLabel) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpAssign opAssign) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpExtend opExtend) {
        String e, f, s, sNodeValues, sMakeNodeValues = "";
        int t, a, b, j;
        List<String> items;
        ArrayList<String> aNodeValues = new ArrayList<String>();

        // split string into an array
        e = opExtend.getVarExprList().getExprs().toString();
        e = e.replaceAll("\\{", "").replaceAll("\\}", "");
        e = e.replaceAll("\\(", "").replaceAll("\\)", "");
        e = e.replaceAll("\\?", "");
        e = e.replaceAll("<", "").replaceAll(">", "");
        e = e.replaceAll("=", " ");
        items = Arrays.asList(e.split(" "));

        // get target variable's index
        t = getIndex(items.get(0));

        // get function name
        f = items.get(1);
        f = f.split("#")[1];

        // get first and second argument's index
        for (int i = 2; i < items.size(); i++) {
            aNodeValues.add("v" + (i - 1));
            j = getIndex(items.get(i));
            if (j > -1) {
                sMakeNodeValues += "		v" + (i - 1) + " = NodeValue.makeNode(t.f" + j + ");\n";
            } else {
                sMakeNodeValues += "		v" + (i - 1) + " = NodeValue.makeNodeString(" + items.get(i) + ");\n";
            }
        }
        sNodeValues = String.join(", ", aNodeValues);


        s = "\n.map(new MapFunction<" +
                sTuple +
                ", " +
                sTuple +
                ">() {\n" +
                "	public " +
                sTuple +
                " map(" +
                sTuple +
                " t) throws Exception {\n" +
                "		" +
                f +
                " op = new " +
                f +
                "();\n" +
                "		NodeValue " +
                sNodeValues +
                ";\n" +
                sMakeNodeValues +
                "		t.setField(op.exec(" +
                sNodeValues +
                "), " +
                t +
                ");\n" +
                "		return t;\n" +
                "	}\n" +
                "})";

        topology.add(s);
    }

    @Override
    public void visit(OpList opList) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpOrder opOrder) {
        //TODO: *IMPL
        // This implementation does not work for DataStream
        ArrayList<String> list = new ArrayList<String>();
        String s;
        Integer i;

        for (SortCondition c : opOrder.getConditions()) {
            s = c.getExpression().toString().substring(1);
            i = getIndex(s);
            s = i + ", " + "Order.";
            if (c.getDirection() < 0) {
                s = s + "DESCENDING";
            } else {
                s = s + "ASCENDING";
            }

            list.add(s);
        }

        for (String j : list) {
            s = "\t\n.sortPartition(";
            s = s + j;
            s = s + ")";

            s = "\t\n.OPNOTIMPL()";
            topology.add(s);
        }
    }

    @Override
    public void visit(OpDistinct opDistinct) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpReduced opReduced) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpSlice opSlice) {
        String s, n = Long.toString(opSlice.getLength());

        s = "\n.flatMap(new FlatMapFunction<" +
                sTuple +
                ", " +
                sTuple +
                ">(){\n" +
                "	public void flatMap(" +
                sTuple +
                " t, Collector<" +
                sTuple +
                "> out) throws Exception {\n" +
                "		if (list.size() < " +
                n +
                ") {\n" +
                "			out.collect(t);\n" +
                "			list.put(list.size() + 1, \" \");\n" +
                "		}\n" +
                "	}\n" +
                "})";

        topology.add(s);
    }

    @Override
    public void visit(OpGroup opGroup) {
        //TODO: *IMPL
        String s = "\t\n.OPNOTIMPL()";

        topology.add(s);
    }

    @Override
    public void visit(OpTopN opTop) {
        //TODO: *IMPL
        // This implementation does not work for DataStream
        ArrayList<String> list = new ArrayList<String>();
        String s;
        Integer i;

        for (SortCondition c : opTop.getConditions()) {
            s = c.getExpression().toString().substring(1);
            i = getIndex(s);
            s = i + ", " + "Order.";
            if (c.getDirection() < 0) {
                s = s + "DESCENDING";
            } else {
                s = s + "ASCENDING";
            }

            list.add(s);
        }

        for (String j : list) {
            s = "\t\n.sortPartition(";
            s = s + j;
            s = s + ")";

            topology.add(s);
        }

        s = "\n.first(";
        String n = Long.toString(opTop.getLimit());
        s = s + n + ")";

        s = "\t\n.OPNOTIMPL()";
        topology.add(s);
    }
}
