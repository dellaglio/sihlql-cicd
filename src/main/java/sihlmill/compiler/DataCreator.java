/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.compiler;

import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.jena.datatypes.MatrixType;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.sparql.graph.GraphFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class DataCreator {

	public static List<Graph> createSimple() {
		List<Graph> input = new ArrayList<Graph>();
		Graph g;


		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/aaron"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/betty")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/aaron"), NodeFactory.createURI("http://example.org/name"), NodeFactory.createLiteral("Aaron")));
		input.add(g);

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/betty"), NodeFactory.createURI("http://example.org/name"), NodeFactory.createLiteral("Betty")));
		input.add(g);

		return input;
	}

	public static List<TimestampedGraph> createIPTVEvents() {
		List<TimestampedGraph> input = new ArrayList<>();
		Graph g;

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("70", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("40", XSDDatatype.XSDint)));
		input.add(new TimestampedGraph(g, 1));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/stopsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("70", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("40", XSDDatatype.XSDint)));
		input.add(new TimestampedGraph(g, 2));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/stopsWatching"), NodeFactory.createURI("http://example.org/tv2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("70", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("40", XSDDatatype.XSDint)));
		input.add(new TimestampedGraph(g, 3));

		// add salary to employees
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/stopsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("70", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("40", XSDDatatype.XSDint)));
		input.add(new TimestampedGraph(g, 4));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/stopsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("70", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("40", XSDDatatype.XSDint)));
		input.add(new TimestampedGraph(g, 5));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/stopsWatching"), NodeFactory.createURI("http://example.org/tv3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("70", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("40", XSDDatatype.XSDint)));
		input.add(new TimestampedGraph(g, 6));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/stopsWatching"), NodeFactory.createURI("http://example.org/tv3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/stopsWatching"), NodeFactory.createURI("http://example.org/tv3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/startsWatching"), NodeFactory.createURI("http://example.org/tv1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("70", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("50", XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("40", XSDDatatype.XSDint)));
		input.add(new TimestampedGraph(g, 7));

		return input;
	}

	public static List<TimestampedGraph> createTimestampedDetections() {
		List<TimestampedGraph> input = new ArrayList<>();
		Graph g;

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		input.add(new TimestampedGraph(g, 1));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		input.add(new TimestampedGraph(g, 2));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		input.add(new TimestampedGraph(g, 3));

		// add salary to employees
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		input.add(new TimestampedGraph(g, 4));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		input.add(new TimestampedGraph(g, 5));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room3")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		input.add(new TimestampedGraph(g, 6));

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room2")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/isIn"), NodeFactory.createURI("http://example.org/room1")));
		input.add(new TimestampedGraph(g, 7));

		//		g = GraphFactory.createGraphMem();
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/enters"), NodeFactory.createURI("http://example.org/room1")));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/leaves"), NodeFactory.createURI("http://example.org/room2")));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/enters"), NodeFactory.createURI("http://example.org/room1")));
		//		input.add(new TimestampedGraph(g, 1));
		//		
		//		g = GraphFactory.createGraphMem();
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/enters"), NodeFactory.createURI("http://example.org/room1")));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/enters"), NodeFactory.createURI("http://example.org/room2")));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/leaves"), NodeFactory.createURI("http://example.org/room1")));
		//		input.add(new TimestampedGraph(g, 2));
		//		
		//		g = GraphFactory.createGraphMem();
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/leaves"), NodeFactory.createURI("http://example.org/room1")));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/enters"), NodeFactory.createURI("http://example.org/room1")));
		//		input.add(new TimestampedGraph(g, 3));
		//		
		//		// add salary to employees
		//		g = GraphFactory.createGraphMem();
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/leaves"), NodeFactory.createURI("http://example.org/room1")));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/leaves"), NodeFactory.createURI("http://example.org/room1")));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/leaves"), NodeFactory.createURI("http://example.org/room2")));
		//		input.add(new TimestampedGraph(g, 4));


		return input;
	}

	public static List<Graph> createCompany() {
		List<Graph> input = new ArrayList<>();
		Graph g;


		// create employees
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("Aaron Arbeiter")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("Betty Bossi")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("Charlie Chef")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("David Delegate")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("Emil Employer")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(20), XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(35), XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(55), XSDDatatype.XSDint)));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("20")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(40), XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(25), XSDDatatype.XSDint)));
		input.add(g);

		// add employees to companies
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/AWK")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/Bethesda")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/AWK")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/AWK")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/Dominos")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/Bethesda")));
		input.add(g);

		// add employees to companies
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/Betty")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/Emil")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/David")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/David")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/Aaron")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/Charlie")));
		input.add(g);

		// add salary to employees
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(80000), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(50000), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(200000), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(80000), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(1000000), XSDDatatype.XSDinteger)));
		input.add(g);

		return input;
	}

	public static List<TimestampedGraph> createTimestampedCompany() {
		List<TimestampedGraph> input = new ArrayList<>();
		Graph g;


		// create employees
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("Aaron Arbeiter")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("Betty Bossi")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("Charlie Chef")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("David Delegate")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/fullname"), NodeFactory.createLiteral("Emil Employer")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(20), XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(35), XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(55), XSDDatatype.XSDint)));
		//		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteral("20")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(40), XSDDatatype.XSDint)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/age"), NodeFactory.createLiteralByValue(new Integer(25), XSDDatatype.XSDint)));
		input.add(new TimestampedGraph(g, 1));

		// add employees to companies
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/AWK")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/Bethesda")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/AWK")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/AWK")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/Dominos")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/worksFor"), NodeFactory.createURI("http://example.org/Bethesda")));
		input.add(new TimestampedGraph(g, 2));

		// add employees to companies
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/Betty")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/Emil")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/David")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/David")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/Aaron")));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/knows"), NodeFactory.createURI("http://example.org/Charlie")));
		input.add(new TimestampedGraph(g, 3));

		// add salary to employees
		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Aaron"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(80000), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Betty"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(50000), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Charlie"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(200000), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/David"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(80000), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI("http://example.org/Emil"), NodeFactory.createURI("http://example.org/earns"), NodeFactory.createLiteral(Integer.toString(1000000), XSDDatatype.XSDinteger)));
		input.add(new TimestampedGraph(g, 4));

		return input;
	}

	public static List<Graph> createMatrix() {
		List<Graph> input = new ArrayList<Graph>();
		Graph g;
		String prefix = "http://example.org/";

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix1"), NodeFactory.createURI(prefix + "data"), NodeFactory.createLiteral("[6 4 8 6][1 0 8 1][9 6 7 3][1 3 8 4]", MatrixType.theMatrixType)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix1"), NodeFactory.createURI(prefix + "columns"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix1"), NodeFactory.createURI(prefix + "rows"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix2"), NodeFactory.createURI(prefix + "data"), NodeFactory.createLiteral("[1 3 6 4][1 3 12 25][0 2 9 0][15 6 3 14]", MatrixType.theMatrixType)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix2"), NodeFactory.createURI(prefix + "columns"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix2"), NodeFactory.createURI(prefix + "rows"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix1"), NodeFactory.createURI(prefix + "evolves"), NodeFactory.createURI(prefix + "matrix2")));
		input.add(g);

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix3"), NodeFactory.createURI(prefix + "data"), NodeFactory.createLiteral("[1 1 1 1][2 0 2 4][9 8 6 8][0 0 6 7]", MatrixType.theMatrixType)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix3"), NodeFactory.createURI(prefix + "columns"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix3"), NodeFactory.createURI(prefix + "rows"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix4"), NodeFactory.createURI(prefix + "data"), NodeFactory.createLiteral("[5 6 5 0][4 2 8 3][6 4 4 2][3 2 7 6]", MatrixType.theMatrixType)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix4"), NodeFactory.createURI(prefix + "columns"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix4"), NodeFactory.createURI(prefix + "rows"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix3"), NodeFactory.createURI(prefix + "evolves"), NodeFactory.createURI(prefix + "matrix4")));
		input.add(g);

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix5"), NodeFactory.createURI(prefix + "data"), NodeFactory.createLiteral("[0 0 2 4][6 4 2 0][9 5 7 24][3 55 2 0]", MatrixType.theMatrixType)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix5"), NodeFactory.createURI(prefix + "columns"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix5"), NodeFactory.createURI(prefix + "rows"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix6"), NodeFactory.createURI(prefix + "data"), NodeFactory.createLiteral("[1 0 5 8][9 3 4 6][0 2 6 4][32 0 8 6]", MatrixType.theMatrixType)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix6"), NodeFactory.createURI(prefix + "columns"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix6"), NodeFactory.createURI(prefix + "rows"), NodeFactory.createLiteral(Integer.toString(4), XSDDatatype.XSDinteger)));
		g.add(Triple.create(NodeFactory.createURI(prefix + "matrix5"), NodeFactory.createURI(prefix + "evolves"), NodeFactory.createURI(prefix + "matrix6")));
		input.add(g);

		return input;
	}

	public static List<TimestampedGraph> readAmazonStream(String filename) {
		System.out.println("Loading data, it can take a while (ca. 60 secs)");

		List<TimestampedGraph> ret = new ArrayList<>();
		String curDir = System.getProperty("user.dir");
		long prevTs = -1;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(curDir+"/"+filename)));
			String line;
			Graph g = null;
			while((line = br.readLine()) != null) {
				String[] data = line.split(",");
				String userUri = "http://example.org/user" + data[0];
				String productUri = "http://example.org/item" + data[1];
				String reviewUri = "http://example.org/review" + data[0] + "-" + data[1];
				String ratingUri = "http://example.org/rating" + data[0] + "-" + data[1];
				long timestamp = Long.parseLong(data[3]);
				if(timestamp > prevTs) {
					if(g!=null) {
						System.out.println(g);
						ret.add(new TimestampedGraph(g, prevTs));
					}
					g = GraphFactory.createDefaultGraph();
					prevTs = timestamp;
				} 
				g.add(Triple.create(                    
						NodeFactory.createURI(reviewUri),
						NodeFactory.createURI("https://schema.org/author"),
						NodeFactory.createURI(userUri)));
				g.add(Triple.create(                    
						NodeFactory.createURI(reviewUri),
						NodeFactory.createURI("https://schema.org/itemReviewed"),
						NodeFactory.createURI(productUri)));
				g.add(Triple.create(                    
						NodeFactory.createURI(reviewUri),
						NodeFactory.createURI("https://schema.org/reviewRating"),
						NodeFactory.createURI(ratingUri)));
				g.add(Triple.create(                    
						NodeFactory.createURI(ratingUri),
						NodeFactory.createURI("https://schema.org/ratingValue"),
						NodeFactory.createLiteral(data[2], XSDDatatype.XSDdouble)));
			}
			System.out.println(g);
			System.out.println("+++++++++++++++++++++");
			ret.add(new TimestampedGraph(g, prevTs));
			return ret;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static String createFunctionCall(String uri) {
    	String[] tmp = uri.substring(7).split("/",2);

    	String functionCall = "DataCreator." + tmp[0]+"(";
    	for(int i = 1; i<tmp.length; i++) {
    		functionCall+="\""+tmp[i]+"\",";
    	}
    	functionCall = functionCall.substring(0, functionCall.length()-1)+")";
		return functionCall;
	}

	public static String localPath(String string) {
		return string;
	}
}
