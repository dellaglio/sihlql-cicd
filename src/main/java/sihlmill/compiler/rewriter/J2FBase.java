/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.compiler.rewriter;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.BasicPattern;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.ExecutionContext;
import org.apache.jena.sparql.expr.Expr;
import org.apache.jena.sparql.expr.ExprAggregator;
import org.apache.jena.sparql.expr.ExprList;
import org.apache.jena.sparql.expr.aggregate.AggCount;
import org.apache.jena.sparql.expr.aggregate.AggCountVar;
import org.apache.jena.sparql.expr.aggregate.AggSum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sihlmill.compiler.DataCreator;
import sihlmill.sihlql.Utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public abstract class J2FBase {
    protected static final Logger logger = LoggerFactory.getLogger(J2FBase.class);

    protected static final int TOP_LEVEL = 0;
    /**
     * Public interface is via QC.execute.
     **/
    // -------- The object starts here --------

    protected ExecutionContext execCxt;
    protected ExecutionDispatch dispatcher = null;
    protected int level = TOP_LEVEL - 1;

    public J2FBase() {//ExecutionContext execCxt)
        //			this.execCxt = execCxt ;
        this.dispatcher = new ExecutionDispatch(this);
        //			this.hideBNodeVars = execCxt.getContext().isTrue(ARQ.hideNonDistiguishedVariables) ;
        //			this.stageGenerator = StageBuilder.chooseStageGenerator(execCxt.getContext()) ;
    }

    public String executeOp(Op op, String input, Vars vs, FileWriter writer, Query q) {
        String ret = exec(op, input, vs, writer);
        ret = writeQueryForm(ret, vs, writer, q);
        return ret;
    }
    
    public Set<String> getSpecificPackages() {return new HashSet<String>();}

    protected String writeQueryForm(String input, Vars vs, FileWriter writer, Query q) {
        Map<Var, Integer> vars = vs.getVars();
        logger.debug("vars{}: {}", vs.getCounter(), vs.getVars());

        long id = System.nanoTime();
        String ret = "ds" + id;
        String returnType = "Tuple" + (vars.size()) + "<TimeAnnotation";
        for (int i = 0; i < vs.getVars().size() - 1; i++) returnType += ",Node";
        returnType += ">";

        try {
            String bp = "";
            if (q.isConstructType()) {
                bp = execute(q.getConstructTemplate().getBGP(), writer);
            }
            writer.write("DataStream<String> " + ret + " = " + input + "\n");
            writer.write(".assignTimestampsAndWatermarks(new AscendingTimestampExtractor<" + returnType + ">(){\n");
            writer.write("@Override\n");
            writer.write("public long extractAscendingTimestamp(" + returnType + " element) {\n");
            writer.write("return element.f0.getTimestamp();\n");
            writer.write("}\n");
            writer.write("})\n");
            writer.write(".timeWindowAll(Time.milliseconds(1))\n");
            if (q.isSelectType()) {
                writer.write(".process(new OutputSelectSerializer<" + returnType + ">(vars" + vs.getCounter() + "));\n");
            } else if (q.isConstructType()) {
                writer.write(".process(new OutputConstructSerializer<" + returnType + ">(" + bp + ", vars" + vs.getCounter() + ", \"" + q.getConstructOutputStream() + "\"));\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return ret;
    }

    protected String exec(Op op, String input, Vars vs, FileWriter writer) {
        level++;
        String ret;
        logger.debug("I'm going to process a node of {}", op.getClass());
        ret = dispatcher.exec(op, input, vs, writer);
        level--;
        //			return qIter ;
        return ret;
    }
    // ---- All the cases

    protected abstract String execute(OpBGP opBGP, String input, Vars vs, FileWriter writer);

    protected void createNewVars(Vars vs, FileWriter writer) throws IOException {
        vs.incCounter();
        writer.write("final Map<Var,Integer> vars" + vs.getCounter() + " = new HashMap<Var,Integer>();\n");
        vs.getVars().forEach((k, v) -> {
            try {
                writer.write("vars" + vs.getCounter() + ".put(Var.alloc(\"" + k.getVarName() + "\")," + v + ");\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    protected String serialize(Node n) {
        if (n.isURI())
            return "NodeFactory.createURI(\"" + n.toString() + "\")";
        if (n.isLiteral()) {
            return "NodeFactory.createLiteral(\"" + n.getLiteralValue().toString() + "\")";
        }
        if (n.isVariable())
            return "Var.alloc(\"" + ((Var) n).getVarName() + "\")";
        throw new RuntimeException();
    }

    protected DataStream<Model> getRDFStream(DataStream<Graph> input) {
        return input.map(new MapFunction<Graph, Model>() {
            @Override
            public Model map(Graph g) throws Exception {
                return ModelFactory.createModelForGraph(g);
            }
        })
                .countWindowAll(10).aggregate(new AggregateFunction<Model, Model, Model>() {
                    @Override
                    public Model add(Model g, Model agg) {
                        return agg.union(g);
                    }

                    @Override
                    public Model createAccumulator() {
                        return ModelFactory.createDefaultModel();
                    }

                    @Override
                    public Model getResult(Model m) {
                        return m;
                    }

                    @Override
                    public Model merge(Model g0, Model g1) {
                        return g0.union(g1);
                    }
                })
                ;
    }

    /*
        protected QueryIterator execute(OpTriple opTriple, QueryIterator input) {
            return execute(opTriple.asBGP(), input) ;
        }
     */
    protected String execute(OpStaticGraphMaterialised opGraph, String input, Vars vs, FileWriter writer) {
        return opGraph.getGraphVarName();
    }

    protected String execute(OpStaticGraph opGraph, String input, Vars vs, FileWriter writer) {
        try {
            writer.write("//before executing the STATICGRAPH block\n");
        } catch (IOException e) {
            logger.error("Writing error: {}", e);
            throw new RuntimeException(e);
        }
        if (opGraph.getSubOp() instanceof OpBGP && ((OpBGP) opGraph.getSubOp()).getPattern().size() == 1) {

            Triple t = ((OpBGP) opGraph.getSubOp()).getPattern().get(0);

            long id = System.nanoTime();
            String ret = "stat" + id;


            Set<Var> vars = Utils.extractVariables(opGraph.getSubOp());
			/*
			Set<Integer> groundPos = new HashSet<>();

//			int varPos = -1;

			if(t.getSubject().isVariable()) {
				vars[0]=(Var) t.getSubject();
			} else
				vars[0] = null;

			if(t.getPredicate().isVariable()) {
				vars[1]=(Var) t.getPredicate();
			} else
				vars[1] = null;

			if(t.getObject().isVariable()) {
				vars[2]=(Var) t.getObject();
			} else
				vars[2] = null;
			 */
            Node graph = opGraph.getNode();
            if (!graph.isURI()) throw new RuntimeException("Not implemented");

            try {
                writer.write("//begin of the STATICGRAPH block\n");

                writer.write("List<Var> vars" + id + " = new ArrayList<>();\n");
                for (Var var : vars) {
                    writer.write("vars" + id + ".add(Var.alloc(\"" + var.getName() + "\"));\n");
                }
                writer.write("StaticBindings " + ret + " = new StaticBindings(vars" + id + ");\n");

                writer.write("BasicPattern tp" + id + " = new BasicPattern();\n");
                ((OpBGP) opGraph.getSubOp()).getPattern().forEach(triple -> {
                    try {
                        writer.write("tp" + id + ".add(Triple.create("
                                + serialize(triple.getSubject()) + ","
                                + serialize(triple.getPredicate()) + ","
                                + serialize(triple.getObject())
                                + "));\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                String gu;
                if(graph.getURI().startsWith("test")) {
                	gu = DataCreator.createFunctionCall(graph.getURI());
                } else {
                	gu = "\""+graph.getURI()+"\"";
                }
                writer.write("Graph data" + id + " = RDFDataMgr.loadGraph(" + gu + ");\n");
                writer.write("Algebra.exec(new OpBGP(tp" + id + "), data" + id + ").forEachRemaining(binding -> " + ret + ".addBinding(binding));\n");
                writer.write("//end of the STATICGRAPH block\n");
                return ret;
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Not implemented");
            }
        } else
            throw new RuntimeException("Not implemented");
    }

    protected String execute(OpGraph opGraph, String input, Vars vs, FileWriter writer) {
        throw new RuntimeException("OpGraph not implemented");
        //			QueryIterator qIter = specialcase(opGraph.getNode(), opGraph.getSubOp(), input) ;
        //			if (qIter != null)
        //				return qIter ;
        //			return new QueryIterGraph(input, opGraph, execCxt) ;
        //		}
        //		private QueryIterator specialcase(Node gn, Op subOp, QueryIterator input) {
        //			// This is a placeholder for code to specially handle explicitly named
        //			// default graph and union graph.
        //
        //			if (Quad.isDefaultGraph(gn)) {
        //				ExecutionContext cxt2 = new ExecutionContext(execCxt, execCxt.getDataset().getDefaultGraph()) ;
        //				return execute(subOp, input, cxt2) ;
        //			}
        //
        //			// Bad news -- if ( Lib.equals(gn, Quad.tripleInQuad) ) {}
        //			return null ;
    }

    /*

        protected QueryIterator execute(OpQuad opQuad, QueryIterator input) {
            return execute(opQuad.asQuadPattern(), input) ;
        }

        protected QueryIterator execute(OpQuadPattern quadPattern, QueryIterator input) {
            // Convert to BGP forms to execute in this graph-centric engine.
            if (quadPattern.isDefaultGraph() && execCxt.getActiveGraph() == execCxt.getDataset().getDefaultGraph()) {
                // Note we tested that the containing graph was the dataset's
                // default graph.
                // Easy case.
                OpBGP opBGP = new OpBGP(quadPattern.getBasicPattern()) ;
                return execute(opBGP, input) ;
            }
            // Not default graph - (graph .... )
            OpBGP opBGP = new OpBGP(quadPattern.getBasicPattern()) ;
            OpGraph op = new OpGraph(quadPattern.getGraphNode(), opBGP) ;
            return execute(op, input) ;
        }

        protected QueryIterator execute(OpQuadBlock quadBlock, QueryIterator input) {
            Op op = quadBlock.convertOp() ;
            return exec(op, input) ;
        }

        protected QueryIterator execute(OpPath opPath, QueryIterator input) {
            return new QueryIterPath(opPath.getTriplePath(), input, execCxt) ;
        }

        protected QueryIterator execute(OpProcedure opProc, QueryIterator input) {
            Procedure procedure = ProcEval.build(opProc, execCxt) ;
            QueryIterator qIter = exec(opProc.getSubOp(), input) ;
            // Delay until query starts executing.
            return new QueryIterProcedure(qIter, procedure, execCxt) ;
        }

        protected QueryIterator execute(OpPropFunc opPropFunc, QueryIterator input) {
            Procedure procedure = ProcEval.build(opPropFunc.getProperty(), opPropFunc.getSubjectArgs(),
                    opPropFunc.getObjectArgs(), execCxt) ;
            QueryIterator qIter = exec(opPropFunc.getSubOp(), input) ;
            return new QueryIterProcedure(qIter, procedure, execCxt) ;
        }
     */
    protected String execute(OpJoinStaticStreamFlatMap opJoin, String input, Vars vs, FileWriter output) {
        String staticBindings = exec(opJoin.getLeft(), input, vs, output);
        long id = System.nanoTime();
        String ret = "ds" + id;

        Set<Var> joinVariables = Utils.extractJoinVariables(opJoin.getLeft(), opJoin.getRight());
        Set<Var> staticOnlyVariables = Utils.extractVariables(opJoin.getLeft());
        staticOnlyVariables.removeAll(joinVariables);

        String streamBindings = exec(opJoin.getRight(), input, vs, output);

        String inputType = "Tuple" + (vs.getVars().size()) + "<TimeAnnotation";
        String returnType = "Tuple" + (vs.getVars().size() + staticOnlyVariables.size()) + "<TimeAnnotation";
        for (int i = 1; i < vs.getVars().size(); i++) {
            inputType += ",Node";
            returnType += ",Node";
        }
        for (int i = 0; i < staticOnlyVariables.size(); i++) returnType += ",Node";
        inputType += ">";
        returnType += ">";

        try {
            output.write("//begin of the STATICSTREAMJOIN-FLATMAP block\n");
            output.write("DataStream<" + returnType + "> " + ret + " = \n" + streamBindings + "\n");
            if (joinVariables.size() > 0) {
                output.write(".flatMap(new StaticStreamJoinFlatMap<" + inputType + ", " + returnType + ">(" + staticBindings + ", vars" + vs.getCounter() + "));\n");
                for (Var v : staticOnlyVariables) {
                    vs.getVars().put(v, vs.getVars().size());
                }
                createNewVars(vs, output);
            }
            output.write("//end of the STATICSTREAMJOIN-FLATMAP block\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    protected String execute(OpJoinStaticStreamFilter opJoin, String input, Vars vs, FileWriter output) {
        String staticBindings = exec(opJoin.getLeft(), input, vs, output);
        try {
            output.write("//before executing the STATICSTREAM-JOIN-FILTER block\n");
        } catch (IOException e) {
            logger.error("Writing error: {}", e);
            throw new RuntimeException(e);
        }
        
        long id = System.nanoTime();
        String ret = "ds" + id;

        Set<Var> joinVariables = Utils.extractJoinVariables(opJoin.getLeft(), opJoin.getRight());

        String streamBindings = exec(opJoin.getRight(), input, vs, output);

        String returnType = "Tuple" + (vs.getVars().size()) + "<TimeAnnotation";
        for (int i = 1; i < vs.getVars().size(); i++) returnType += ",Node";
        returnType += ">";

        try {
            output.write("//begin of the STATICSTREAM-JOIN-FILTER block\n");
            output.write("DataStream<" + returnType + "> " + ret + " = \n" + streamBindings + "\n");
            output.write(".filter(new StaticStreamJoinFilter<" + returnType + ">(" + staticBindings + ", vars" + vs.getCounter() + "))\n"
                    + ".returns(TypeInformation.of(new TypeHint<" + returnType + ">(){}));\n");
            output.write("//end of the STATICSTREAM-JOIN-FILTER block\n");
       } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    protected String execute(OpJoin opJoin, String input, Vars vs, FileWriter output) {
        logger.error("it should not open this method");
        return input;

    }

    /*
        // Pass iterator from one step directly into the next.
        protected QueryIterator execute(OpSequence opSequence, QueryIterator input) {
            QueryIterator qIter = input ;
            for (Iterator<Op> iter = opSequence.iterator(); iter.hasNext();) {
                Op sub = iter.next() ;
                qIter = exec(sub, qIter) ;
            }
            return qIter ;
        }
     */
    protected String execute(OpLeftJoinStaticStreamFlatMap opLeftJoin, String input, Vars vs, FileWriter output) {
        try {
            output.write("//before executing the STATICSTREAM-LEFTJOIN-FLATMAP block\n");
        } catch (IOException e) {
            logger.error("Writing error: {}", e);
            throw new RuntimeException(e);
        }
        String staticBindings = exec(opLeftJoin.getLeft(), input, vs, output);
        long id = System.nanoTime();
        String ret = "ds" + id;

        Set<Var> joinVariables = Utils.extractJoinVariables(opLeftJoin.getLeft(), opLeftJoin.getRight());
        Set<Var> staticOnlyVariables = Utils.extractVariables(opLeftJoin.getLeft());
        staticOnlyVariables.removeAll(joinVariables);

        String streamBindings = exec(opLeftJoin.getRight(), input, vs, output);

        String inputType = "Tuple" + (vs.getVars().size()) + "<TimeAnnotation";
        String returnType = "Tuple" + (vs.getVars().size() + staticOnlyVariables.size()) + "<TimeAnnotation";
        for (int i = 1; i < vs.getVars().size(); i++) {
            inputType += ",Node";
            returnType += ",Node";
        }
        for (int i = 0; i < staticOnlyVariables.size(); i++) returnType += ",Node";
        inputType += ">";
        returnType += ">";

        try {
            output.write("//begin of the STATICSTREAM-LEFTJOIN-FLATMAP block\n");
            output.write("DataStream<" + returnType + "> " + ret + " = \n" + streamBindings + "\n");
            if (joinVariables.size() > 0) {
                output.write(".flatMap(new StaticStreamLeftJoinFlatMap<" + inputType + ", " + returnType + ">(" + staticBindings + ", vars" + vs.getCounter() + "))\n"
                        + " .returns(TypeInformation.of(new TypeHint<" + returnType + ">(){}))"
                        + ";\n");
                for (Var v : staticOnlyVariables) {
                    vs.getVars().put(v, vs.getVars().size());
                }
                createNewVars(vs, output);
            }
            output.write("//end of the STATICSTREAM-LEFTJOIN-FLATMAP block\n");
            
            //FIXME: can be improved by pushing the filter before the join, but needs to check the variables
            if(opLeftJoin.getExprs() != null && opLeftJoin.getExprs().size() > 0) {
            	ret = exec(OpFilter.filterBy(opLeftJoin.getExprs(), OpNull.create()), ret, vs, output);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    //	protected String execute(OpLeftJoinStaticStreamFilter opLeftJoin, String input, Vars vs, FileWriter output) {
//		String staticBindings = exec(opLeftJoin.getLeft(), input, vs, output) ;
//		long id = System.nanoTime();
//		String ret = "ds"+id;
//
//		Set<Var> joinVariables = Utils.extractJoinVariables(opLeftJoin.getLeft(), opLeftJoin.getRight());
//
//		String streamBindings = exec(opLeftJoin.getRight(), input, vs, output);
//
//		String returnType = "Tuple"+(vs.getVars().size())+"<TimeAnnotation";
//		for(int i=1; i<vs.getVars().size(); i++) returnType += ",Node";
//		returnType+= ">";
//
//		try {
//
//			output.write("DataStream<"+returnType+"> " + ret + " = \n" + streamBindings + "\n");
//			output.write(".filter(new StaticStreamJoinFilter<"+returnType+">("+staticBindings+", vars"+vs.getCounter()+"))\n"
//					+ ".returns(TypeInformation.of(new TypeHint<"+returnType+">(){}));\n");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return ret;
//	}
    protected String execute(OpLeftJoin opLeftJoin, String input, Vars vs, FileWriter writer) {
        logger.error("it should not open the method having {} as op", opLeftJoin.getClass());
        throw new RuntimeException("it should not open the method having " + opLeftJoin.getClass() + " as op");
//		return input;

/*		System.out.println("aaaa");
		Map<Var,Integer> vars = vs.getVars();

		long id = System.nanoTime();
		String ret = "ds"+id;
		String returnType = "Tuple"+vars.size()+"<TimeAnnotation";
		for(int i=0; i<vars.size()-1; i++) returnType += ",Node";
		returnType+= ">";

		Op left = opLeftJoin.getLeft() ;
		String lStream = exec(left, input, vs, writer) ;

		Op right = opLeftJoin.getRight() ;
		String rStream = exec(left, input, vs, writer) ;

		try {

			writer.write("vars.clear();\n");
			vars.forEach((k,v) -> {
				try {
					writer.write("vars.put(Var.alloc(\""+k.getVarName()+"\"),"+v+");\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			});

			writer.write("DataStream<"+returnType+"> "+ret+"="+lStream+"\n");
			writer.write(";\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		//			DataStream ret = rdfStream.flatMap(bgpe).returns(new TypeHint() {
		//				@Override
		//				public TypeInformation getTypeInfo() {
		//					return TypeInformation.of(Tuple.getTupleClass(vars.size()));
		//				}
		//			});
		return ret;
*/
    }

    /*
        protected QueryIterator execute(OpConditional opCondition, QueryIterator input) {
            QueryIterator left = exec(opCondition.getLeft(), input) ;
            QueryIterator qIter = new QueryIterOptionalIndex(left, opCondition.getRight(), execCxt) ;
            return qIter ;
        }

        protected QueryIterator execute(OpDiff opDiff, QueryIterator input) {
            QueryIterator left = exec(opDiff.getLeft(), input) ;
            QueryIterator riimport ...function.BGPEval;
ght = exec(opDiff.getRight(), root()) ;
            return new QueryIterDiff(left, right, execCxt) ;
        }

        protected QueryIterator execute(OpMinus opMinus, QueryIterator input) {
            Op lhsOp = opMinus.getLeft() ;
            Op rhsOp = opMinus.getRight() ;

            QueryIterator left = exec(lhsOp, input) ;
            QueryIterator right = exec(rhsOp, root()) ;

            Set<Var> commonVars = OpVars.visibleVars(lhsOp) ;
            commonVars.retainAll(OpVars.visibleVars(rhsOp)) ;

            return QueryIterMinus.create(left, right, commonVars, execCxt) ;
        }

        protected QueryIterator execute(OpDisjunction opDisjunction, QueryIterator input) {
            QueryIterator cIter = new QueryIterUnion(input, opDisjunction.getElements(), execCxt) ;
            return cIter ;
        }

        protected QueryIterator execute(OpUnion opUnion, QueryIterator input) {
            List<Op> x = flattenUnion(opUnion) ;
            QueryIterator cIter = new QueryIterUnion(input, x, execCxt) ;
            return cIter ;
        }

        // Based on code from Olaf Hartig.
        protected List<Op> flattenUnion(OpUnion opUnion) {
            List<Op> x = new ArrayList<>() ;
            flattenUnion(x, opUnion) ;
            return x ;
        }

        protected void flattenUnion(List<Op> acc, OpUnion opUnion) {
            if (opUnion.getLeft() instanceof OpUnion)
                flattenUnion(acc, (OpUnion)opUnion.getLeft()) ;
            else
                acc.add(opUnion.getLeft()) ;

            if (opUnion.getRight() instanceof OpUnion)
                flattenUnion(acc, (OpUnion)opUnion.getRight()) ;
            else
                acc.add(opUnion.getRight()) ;
        }
     */
    protected String execute(OpFilter opFilter, String input, Vars vs, FileWriter writer) {
        Map<Var, Integer> vars = vs.getVars();

        long id = System.nanoTime();
        String ret = "ds" + id;
        String returnType = "Tuple" + vars.size() + "<TimeAnnotation";
        for (int i = 0; i < vars.size() - 1; i++) returnType += ",Node";
        returnType += ">";

        try {
            writer.write("//before executing the FILTER block\n");
        } catch (IOException e) {
            logger.error("Writing error: {}", e);
            throw new RuntimeException(e);
        }
        Op base = opFilter.getSubOp();
        String subStream = exec(base, input, vs, writer);

        try {
            writer.write("//begin of the FILTER block\n");
            writer.write("DataStream<" + returnType + "> " + ret + "=" + subStream + "\n");
            ExprList exprs = opFilter.getExprs();
            for (Expr expr : exprs) {
                writer.write(".filter(new FilterFunction<" + returnType + ">() {\n" +
                        "\n" +
                        "@Override\n" +
                        "public boolean filter(" + returnType + " e) throws Exception {\n" +
                        "Expr expr = "
                );
                expr.visit(new J2FExprVisitor(writer));
                writer.write(";\n"
                        + "FunctionEnv fe = new FunctionEnvBase();\n" +
                        "BindingHashMap bhm = new BindingHashMap();\n" +
                        "vars"+vs.getCounter()+".forEach((v,i) -> {if(i>0) bhm.add(v, e.getField(i));});\n" +
                        "NodeValue v = expr.eval(bhm, fe);\n" +
                        "return v.getBoolean();\n" +
                        "}\n" +
                        "} // "
                        + expr.toString()
                        + "\n)\n");


            }
            writer.write(";\n");
            writer.write("//end of the FILTER block\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //			DataStream ret = rdfStream.flatMap(bgpe).returns(new TypeHint() {
        //				@Override
        //				public TypeInformation getTypeInfo() {
        //					return TypeInformation.of(Tuple.getTupleClass(vars.size()));
        //				}
        //			});
        return ret;
    }

    /*
        protected QueryIterator execute(OpService opService, QueryIterator input) {
            return new QueryIterService(input, opService, execCxt) ;
        }

        // Quad form, "GRAPH ?g {}" Flip back to OpGraph.
        // Normally quad stores override this.
        protected QueryIterator execute(OpDatasetNames dsNames, QueryIterator input) {
            if (false) {
                OpGraph op = new OpGraph(dsNames.getGraphNode(), new OpBGP()) ;
                return execute(op, input) ;
            }
            throw new ARQNotImplemented("execute/OpDatasetNames") ;
        }

        protected QueryIterator execute(OpTable opTable, QueryIterator input) {
            if (opTable.isJoinIdentity())
                return input ;
            if (input.isJoinIdentity() ) {
                input.close() ;
                return opTable.getTable().iterator(execCxt);
            }
            QueryIterator qIterT = opTable.getTable().iterator(execCxt) ;
            QueryIterator qIter = Join.join(input, qIterT, execCxt) ;
            return qIter ;
        }

        protected QueryIterator execute(OpExt opExt, QueryIterator input) {
            try {
                QueryIterator qIter = opExt.eval(input, execCxt) ;
                if (qIter != null)
                    return qIter ;
            } catch (UnsupportedOperationException ex) {}
            // null or UnsupportedOperationException
            throw new QueryExecException("Encountered unsupported OpExt: " + opExt.getName()) ;
        }

        protected QueryIterator execute(OpLabel opLabel, QueryIterator input) {
            if (!opLabel.hasSubOp())
                return input ;

            return exec(opLabel.getSubOp(), input) ;
        }

        protected QueryIterator execute(OpNull opNull, QueryIterator input) {
            // Loose the input.
            input.close() ;
            return QueryIterNullIterator.create(execCxt) ;
        }

        protected QueryIterator execute(OpList opList, QueryIterator input) {
            return exec(opList.getSubOp(), input) ;
        }

        protected QueryIterator execute(OpOrder opOrder, QueryIterator input) {
            QueryIterator qIter = exec(opOrder.getSubOp(), input) ;
            qIter = new QueryIterSort(qIter, opOrder.getConditions(), execCxt) ;
            return qIter ;
        }

        protected QueryIterator execute(OpTopN opTop, QueryIterator input) {
            QueryIterator qIter = null ;
            // We could also do (reduced) here as well.
            // but it's detected in TransformTopN and turned into (distinct)
            // there so that code catches that already.
            // We leave this to do the strict case of (top (distinct ...))
            if (opTop.getSubOp() instanceof OpDistinct) {
                OpDistinct opDistinct = (OpDistinct)opTop.getSubOp() ;
                qIter = exec(opDistinct.getSubOp(), input) ;
                qIter = new QueryIterTopN(qIter, opTop.getConditions(), opTop.getLimit(), true, execCxt) ;
            } else {
                qIter = exec(opTop.getSubOp(), input) ;
                qIter = new QueryIterTopN(qIter, opTop.getConditions(), opTop.getLimit(), false, execCxt) ;
            }
            return qIter ;
        }
     */
    protected String execute(OpProject opProject, String input, Vars vs, FileWriter writer) {
        // This may be under a (graph) in which case we need to operate
        // on the active graph.

        // More intelligent QueryIterProject needed.
        try {
            writer.write("//before executing the PROJECT block\n");
        } catch (IOException e) {
            logger.error("Writing error: {}", e);
            throw new RuntimeException(e);
        }
        String subStream = exec(opProject.getSubOp(), input, vs, writer);

        Map<Var, Integer> vars = vs.getVars();
        vs.incCounter();

        Map<Var, Integer> newVars = new HashMap<>();
        newVars.put(Var.alloc(".timestamp"), 0);
        if (opProject.getVars().size() != vars.size() - 1) {
            long id = System.nanoTime();
            String ret = "ds" + id;
            String returnType = "Tuple" + (1 + opProject.getVars().size()) + "<TimeAnnotation";
            for (int i = 0; i < opProject.getVars().size(); i++) returnType += ",Node";
            returnType += ">";

            try {
                writer.write("//begin of the PROJECT block\n");
                writer.write("DataStream<" + returnType + "> " + ret + " = " + subStream + ".project(0,");
                String newVarString = "vars" + vs.getCounter() + ".put(Var.alloc(\".timestamp\"),0);\n";
                for (int i = 0; i < opProject.getVars().size(); i++) {
                    Var v = opProject.getVars().get(i);
                    newVarString += "vars" + vs.getCounter() + ".put(Var.alloc(\"" + v.getVarName() + "\")," + (i + 1) + ");\n";
                    newVars.put(v, vars.get(v));
                    writer.write(Integer.toString(vars.get(v)));
                    if (i + 1 != opProject.getVars().size())
                        writer.write(",");
                }
                writer.write(");\n");
                writer.write("final Map<Var,Integer> vars" + vs.getCounter() + "= new HashMap<Var,Integer>();\n");
                writer.write(newVarString);
                vs.setVars(newVars);
                writer.write("//end of the PROJECT block\n");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return ret;
        } else
            return subStream;

    }

    /*
        protected QueryIterator execute(OpSlice opSlice, QueryIterator input) {
            QueryIterator qIter = exec(opSlice.getSubOp(), input) ;
            qIter = new QueryIterSlice(qIter, opSlice.getStart(), opSlice.getLength(), execCxt) ;
            return qIter ;
        }
     */
    protected abstract String execute(OpGroup opGroup, String input, Vars vs, FileWriter writer);

    /*
        protected QueryIterator execute(OpDistinct opDistinct, QueryIterator input) {
            QueryIterator qIter = exec(opDistinct.getSubOp(), input) ;
            qIter = new QueryIterDistinct(qIter, execCxt) ;
            return qIter ;
        }

        protected QueryIterator execute(OpReduced opReduced, QueryIterator input) {
            QueryIterator qIter = exec(opReduced.getSubOp(), input) ;
            qIter = new QueryIterReduced(qIter, execCxt) ;
            return qIter ;
        }

        protected QueryIterator execute(OpAssign opAssign, QueryIterator input) {
            QueryIterator qIter = exec(opAssign.getSubOp(), input) ;
            qIter = new QueryIterAssign(qIter, opAssign.getVarExprList(), execCxt, false) ;
            return qIter ;
        }
     */
    protected String execute(OpExtend opExtend, String input, Vars vs, FileWriter writer) {
        // We know (parse time checking) the variable is unused so far in
        // the query so we can use QueryIterAssign knowing that it behaves
        // the same as extend. The boolean should only be a check.
        try {
            writer.write("//before executing the EXTEND block\n");
        } catch (IOException e) {
            logger.error("Writing error: {}", e);
            throw new RuntimeException(e);
        }
        String subStream = exec(opExtend.getSubOp(), input, vs, writer);

        final Map<Var, Integer> vars = vs.getVars();

        long id = System.nanoTime();
        String ret = "ds" + id;

        opExtend.getVarExprList().forEachExpr(new BiConsumer<Var, Expr>() {
            @Override
            public void accept(Var t, Expr u) {
                try {
                    writer.write("//begin of the EXTEND block\n");
                    if (u.isFunction()) {
                        String dataStreamReturnType = "TimeAnnotation";
                        for (int i = 0; i < vars.size(); i++)
                            dataStreamReturnType += ",Node";
                        writer.write("DataStream<Tuple" + (vars.size() + 1) + "<" + dataStreamReturnType + ">> " + ret + " = " + subStream + "\n.map("
                                + "new MapFunction<Tuple" + (vars.size()) + "<TimeAnnotation");
                        for (int i = 1; i < vars.size(); i++)
                            writer.write(",Node");
                        writer.write(">, Tuple" + (vars.size() + 1) + "<" + dataStreamReturnType + ">>() {\n");
                        writer.write("@Override\n" +
                                "public Tuple" + (vars.size() + 1) + "<" + dataStreamReturnType + "> map(Tuple" + (vars.size()) + "<TimeAnnotation");
                        for (int i = 1; i < vars.size(); i++)
                            writer.write(",Node");
                        writer.write("> value) throws Exception {\n");
                        writer.write("Expr expr = ");
                        u.getFunction().visit(new J2FExprVisitor(writer));
                        writer.write(";\n" +
                                "FunctionEnv fe = new FunctionEnvBase();\n" +
                                "BindingHashMap bhm = new BindingHashMap();\n" +
                                "vars" + vs.getCounter() + ".forEach((v,i) -> {if(value.getField(i) instanceof Node) bhm.add(v, value.getField(i));});\n" +
                                "NodeValue v = expr.eval(bhm, fe);\n");
                        writer.write("return new Tuple" + (vars.size() + 1) + "<" + dataStreamReturnType + ">(\n");
                        for (int i = 0; i < vars.size(); i++)
                            writer.write("value.f" + i + ",");
                        writer.write("v.asNode());\n");
                        writer.write("}\n});\n");

                        vs.getVars().put(t, vs.getVars().size());
                        createNewVars(vs, writer);
                    } else if (u.isVariable()) {
                        String dataStreamReturnType = "TimeAnnotation";
                        for (int i = 0; i < vars.size() - 1; i++)
                            dataStreamReturnType += ",Node";
                        writer.write("DataStream<Tuple" + (vars.size()) + "<" + dataStreamReturnType + ">> " + ret + " = " + subStream + ";\n");
                        Integer i = vars.get(u.asVar());
                        try {
                            vars.put(t, i);
                            vars.remove(u.asVar());
                            createNewVars(vs, writer);
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } else {
                        throw new RuntimeException("Not implemented");
                    }
                    writer.write("//end of the EXTEND block\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return ret;
    }

    protected String execute(BasicPattern bp, FileWriter writer) {
        long id = System.nanoTime();
        String ret = "tp" + id;
        try {
            writer.write("List<Triple> " + ret + " = new ArrayList<>();\n");
            bp.forEach(t -> {
                try {
                    writer.write("tp" + id + ".add(Triple.create("
                            + serialize(t.getSubject()) + ","
                            + serialize(t.getPredicate()) + ","
                            + serialize(t.getObject())
                            + "));\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return ret;

    }

	/*
		public static QueryIterator createRootQueryIterator(ExecutionContext execCxt) {
			return QueryIterRoot.create(execCxt) ;
		}

		protected QueryIterator root() {
			return createRootQueryIterator(execCxt) ;
		}

		// Use this to debug evaluation
		// Example:
		// input = debug(input) ;
		private QueryIterator debug(String marker, QueryIterator input) {
			List<Binding> x = all(input) ;
			for (Binding b : x) {
				System.out.print(marker) ;
				System.out.print(": ") ;
				System.out.println(b) ;
			}

			return new QueryIterPlainWrapper(x.iterator(), execCxt) ;
		}

		private static List<Binding> all(QueryIterator input) {
			return Iter.toList(input) ;
		}
	 */
}
