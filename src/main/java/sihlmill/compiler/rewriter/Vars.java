/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.compiler.rewriter;

import org.apache.jena.sparql.core.Var;

import java.util.HashMap;
import java.util.Map;

public class Vars {
    private Map<Var, Integer> vars;
    private Integer counter;

    public Vars() {
        super();
        counter = 0;
        vars = new HashMap<Var, Integer>();
        vars.put(Var.alloc(".timestamp"), 0);
    }

    public Map<Var, Integer> getVars() {
        return vars;
    }

    public void setVars(Map<Var, Integer> vars) {
        this.vars = vars;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public void incCounter() {
        this.counter++;
    }
}
