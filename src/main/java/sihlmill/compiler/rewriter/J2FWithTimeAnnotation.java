/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.compiler.rewriter;

import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.algebra.optimize.VariableUsageTracker;
import org.apache.jena.sparql.algebra.optimize.VariableUsageVisitor;
import org.apache.jena.sparql.core.Var;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public abstract class J2FWithTimeAnnotation extends J2FBase {
    private static final Logger logger = LoggerFactory.getLogger(J2FWithTimeAnnotation.class);

    @Override
    protected String execute(OpBGP opBGP, String input, Vars vs, FileWriter writer) {
        long id = System.nanoTime();
        //			DataStream<Model> rdfStream = getRDFStream(input);
        String ret = "ds" + id;

        Map<Var, Integer> vars = vs.getVars();

        VariableUsageTracker vut = new VariableUsageTracker();
        opBGP.visit(new VariableUsageVisitor(vut) {
            @Override
            protected void action(Collection<Var> variables) {
                variables.forEach(var -> {
                    if (!vars.containsKey(var)) vars.put(var, vars.size());
                });
            }

            @Override
            protected void action(Var var) {
                if (!vars.containsKey(var))
                    vars.put(var, vars.size());
            }

            @Override
            protected void action(String var) {
            }

        });

        String returnType = "Tuple" + vars.size() + "<TimeAnnotation";
        for (int i = 0; i < vars.size() - 1; i++) returnType += ",Node";
        returnType += ">";

        try {
            writer.write("//begin of the BGP block\n");
            createNewVars(vs, writer);
            String tp = execute(opBGP.getPattern(), writer);
            writer.write("BGPEvalDisj<" + returnType + "> bgpe" + id + " = new BGPEvalDisj<>(" + tp + ", vars" + vs.getCounter() + ");\n");
            writer.write("DataStream<" + returnType + "> " + ret + " = " + input + "\n"
                    + ".flatMap(bgpe" + id + ")\n"
              		+ ".returns(new TypeHint<" + returnType + ">() {});\n");
            writer.write("//end of the BGP block\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
