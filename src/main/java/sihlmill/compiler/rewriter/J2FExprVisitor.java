/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.compiler.rewriter;

import org.apache.jena.sparql.expr.*;
import org.apache.jena.sparql.expr.nodevalue.NodeValueDecimal;

import java.io.FileWriter;
import java.io.IOException;

public class J2FExprVisitor implements ExprVisitor {
    private FileWriter writer;

    public J2FExprVisitor(FileWriter writer) {
        this.writer = writer;
    }

    @Override
    public void visit(ExprFunction2 func) {
        try {
            writer.write("new " + func.getClass().getCanonicalName() + "(");
            func.getArg1().visit(this);
            writer.write(",");
            func.getArg2().visit(this);
            writer.write(")");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void visit(ExprVar nv) {
        try {
            writer.write("new ExprVar(\"" + nv.getVarName() + "\")");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void visit(ExprFunction0 func) {
        try {
            writer.write("new " + func.getClass().getCanonicalName() + "()");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void visit(ExprFunction1 func) {
        try {
            writer.write("new " + func.getClass().getCanonicalName() + "(");
            func.getArg().visit(this);
            writer.write(")");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void visit(ExprFunction3 func) {
        try {
            writer.write("new " + func.getClass().getCanonicalName() + "(");
            func.getArg1().visit(this);
            writer.write(",");
            func.getArg2().visit(this);
            writer.write(",");
            func.getArg3().visit(this);
            writer.write(")");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void visit(ExprFunctionN func) {
        try {
            writer.write("new " + func.getClass().getCanonicalName() + "(");
            func.getArgs().get(0).visit(this);
            for (int i = 1; i < func.getArgs().size(); i++) {
                writer.write(",");
                func.getArgs().get(i).visit(this);
            }
            if (func instanceof E_Regex) {
                writer.write(",null");
            }
            writer.write(")");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void visit(ExprFunctionOp funcOp) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(NodeValue nv) {
        try {
            writer.write("new " + nv.getClass().getCanonicalName() + "(");
            if(nv instanceof NodeValueDecimal)
            	writer.write("new BigDecimal(");
            writer.write(nv.toString());
            if(nv instanceof NodeValueDecimal)
            	writer.write(")");
            writer.write(")");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void visit(ExprAggregator eAgg) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(ExprNone exprNone) {
        throw new RuntimeException("not implemented");
    }
}
