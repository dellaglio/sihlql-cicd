/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.compiler.rewriter;

import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.core.Var;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public abstract class J2FWithoutTimeAnnotation extends J2FBase {
    protected String execute(OpBGP opBGP, String input, Vars vs, FileWriter writer) {
        long id = System.nanoTime();
        //			DataStream<Model> rdfStream = getRDFStream(input);
        String ret = "ds" + id;

        Map<Var, Integer> vars = vs.getVars();

        String returnType = "Tuple" + vars.size() + "<TimeAnnotation";
        for (int i = 0; i < vars.size() - 1; i++) returnType += ",Node";
        returnType += ">";

        try {
            createNewVars(vs, writer);
            //			writer.write("final Map<Var,Integer> vars"+vs.getCounter()+" = new HashMap<Var,Integer>();\n");
            //			vars.forEach((k,v) -> {
            //				try {
            //					writer.write("vars"+vs.getCounter()+".put(Var.alloc(\""+k.getVarName()+"\"),"+v+");\n");
            //				} catch (IOException e) {
            //					e.printStackTrace();
            //				}
            //			});
            //			BasicPattern bp = new BasicPattern();
            String bp = execute(opBGP.getPattern(), writer);

            writer.write("BGPEval<" + returnType + "> bgpe" + id + " = new BGPEval<>(" + bp + ", vars" + vs.getCounter() + ");\n");
            writer.write("DataStream<" + returnType + "> " + ret + "=" + input + ""
                    + ".assignTimestampsAndWatermarks(new AscendingTimestampExtractor<TimestampedGraph>(){\n"
                    + "@Override\n"
                    + "public long extractAscendingTimestamp(TimestampedGraph element) {\n"
                    + "return element.getTimestamp();\n"
                    + "}\n"
                    + "})"
                    + ".map(new TimestampedGraphToModel())\n"
                    + ".countWindowAll(2)\n"
                    + ".aggregate(new WindowMerger())\n"
                    + ".flatMap(bgpe" + id + ")\n"
                    + ".returns(new TypeHint<" + returnType + ">() {});\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

}
