/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package sihlmill.compiler.rewriter;

import org.apache.jena.atlas.logging.Log;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.OpVisitor;
import org.apache.jena.sparql.algebra.op.*;

import java.io.FileWriter;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Class to provide type-safe execution dispatch using the visitor support of Op
 */

class ExecutionDispatch implements OpVisitor {
    private Deque<String> stack = new ArrayDeque<>();
    private J2FBase opExecutor;
    private Vars vars;
    private FileWriter writer;

    ExecutionDispatch(J2FBase exec) {
        opExecutor = exec;
    }

    String exec(Op op, String input, Vars vars, FileWriter writer) {
        push(input);
        int x = stack.size();
        this.vars = vars;
        this.writer = writer;
        op.visit(this);
        int y = stack.size();
        if (x != y)
            Log.warn(this, "Possible stack misalignment");
        String qIter = pop();
        return qIter;
    }

    @Override
    public void visit(OpBGP opBGP) {
        String input = pop();
        String str = opExecutor.execute(opBGP, input, vars, writer);
        push(str);
    }

    /*
       @Override
       public void visit(OpQuadPattern quadPattern)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(quadPattern, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpQuadBlock quadBlock)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(quadBlock, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpTriple opTriple)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(opTriple, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpQuad opQuad)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(opQuad, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpPath opPath)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(opPath, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpProcedure opProc)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(opProc, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpPropFunc opPropFunc)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(opPropFunc, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpJoin opJoin)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(opJoin, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpSequence opSequence)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(opSequence, input, null) ;
           push(qIter) ;
       }

       @Override
       public void visit(OpDisjunction opDisjunction)
       {
           DataStream input = pop() ;
           DataStream qIter = opExecutor.execute(opDisjunction, input, null) ;
           push(qIter) ;
       }

   */
    @Override
    public void visit(OpLeftJoin opLeftJoin) {
        String input = pop();
        String qIter;
//        if(opLeftJoin instanceof OpLeftJoinStaticStreamFilter)
//            qIter = opExecutor.execute(((OpLeftJoinStaticStreamFilter)opLeftJoin), input, vars, writer) ;
//        else 
        if (opLeftJoin instanceof OpLeftJoinStaticStreamFlatMap)
            qIter = opExecutor.execute((OpLeftJoinStaticStreamFlatMap) opLeftJoin, input, vars, writer);
        else
            qIter = opExecutor.execute(opLeftJoin, input, vars, writer);
        push(qIter);
    }

    /*
        @Override
        public void visit(OpDiff opDiff)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opDiff, input, null) ;
            push(qIter) ;
        }

        @Override
        public void visit(OpMinus opMinus)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opMinus, input, null) ;
            push(qIter) ;
        }

        @Override
        public void visit(OpUnion opUnion)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opUnion, input, null) ;
            push(qIter) ;
        }

        @Override
        public void visit(OpConditional opCondition)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opCondition, input, null) ;
            push(qIter) ;
        }
    */
    @Override
    public void visit(OpFilter opFilter) {
        String input = pop();
        String str = opExecutor.execute(opFilter, input, vars, writer);
        push(str);
    }

    @Override
    public void visit(OpGraph opGraph) {
        String input = pop();
        String str = null;
        if (opGraph instanceof OpStaticGraphMaterialised)
            str = opExecutor.execute(((OpStaticGraphMaterialised) opGraph), input, vars, writer);
        else if (opGraph instanceof OpStaticGraph)
            str = opExecutor.execute(((OpStaticGraph) opGraph), input, vars, writer);
        else
            str = opExecutor.execute(opGraph, input, vars, writer);
        push(str);
    }

    /*
    @Override
    public void visit(OpService opService)
    {
        DataStream input = pop() ;
        DataStream qIter = opExecutor.execute(opService, input, null) ;
        push(qIter) ;
    }

    @Override
    public void visit(OpDatasetNames dsNames)
    {
        DataStream input = pop() ;
        DataStream qIter = opExecutor.execute(dsNames, input, null) ;
        push(qIter) ;
    }

    @Override
    public void visit(OpTable opTable)
    {
        DataStream input = pop() ;
        DataStream qIter = opExecutor.execute(opTable, input, null) ;
        push(qIter) ;
    }

    @Override
    public void visit(OpExt opExt)
    {
        DataStream input = pop() ;
        DataStream qIter = opExecutor.execute(opExt, input, null) ;
        push(qIter) ;
    }

    @Override
    public void visit(OpNull opNull)
    {
        DataStream input = pop() ;
        DataStream qIter = opExecutor.execute(opNull, input, null) ;
        push(qIter) ;
    }

    @Override
    public void visit(OpLabel opLabel)
    {
        DataStream input = pop() ;
        DataStream qIter = opExecutor.execute(opLabel, input, null) ;
        push(qIter) ;
    }

    @Override
    public void visit(OpList opList)
    {
        DataStream input = pop() ;
        DataStream qIter = opExecutor.execute(opList, input, null) ;
        push(qIter) ;
    }

    @Override
    public void visit(OpOrder opOrder)
    {
        DataStream input = pop() ;
        DataStream qIter = opExecutor.execute(opOrder, input, null) ;
        push(qIter) ;
    }
*/
    @Override
    public void visit(OpProject opProject) {
        String input = pop();
        String qIter = opExecutor.execute(opProject, input, vars, writer);
        push(qIter);
    }

    /*
        @Override
        public void visit(OpDistinct opDistinct)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opDistinct, input, null) ;
            push(qIter) ;
        }

        @Override
        public void visit(OpReduced opReduced)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opReduced, input, null) ;
            push(qIter) ;
        }

        @Override
        public void visit(OpAssign opAssign)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opAssign, input, null) ;
            push(qIter) ;
        }
    */
    @Override
    public void visit(OpExtend opExtend) {
        String input = pop();
        String qIter = opExecutor.execute(opExtend, input, vars, writer);
        push(qIter);
    }

    /*
        @Override
        public void visit(OpSlice opSlice)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opSlice, input, null) ;
            push(qIter) ;
        }
    */
    @Override
    public void visit(OpGroup opGroup) {
        String input = pop();
        String qIter = opExecutor.execute(opGroup, input, vars, writer);
        push(qIter);
    }

    /*
        @Override
        public void visit(OpTopN opTop)
        {
            DataStream input = pop() ;
            DataStream qIter = opExecutor.execute(opTop, input, null) ;
            push(qIter) ;
        }
    */
    private void push(String qIter) {
        stack.push(qIter);
    }

    private String pop() {
        if (stack.size() == 0)
            Log.warn(this, "Warning: pop: empty stack");
        return stack.pop();
    }

    @Override
    public void visit(OpQuadPattern quadPattern) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpQuadBlock quadBlock) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpTriple opTriple) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpQuad opQuad) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpPath opPath) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpTable opTable) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpNull opNull) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpProcedure opProc) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpPropFunc opPropFunc) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpService opService) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpDatasetNames dsNames) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpLabel opLabel) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpAssign opAssign) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpJoin opJoin) {
        String input = pop();
        String qIter = null;
        if (opJoin instanceof OpJoinStaticStreamFilter)
            qIter = opExecutor.execute(((OpJoinStaticStreamFilter) opJoin), input, vars, writer);
        else if (opJoin instanceof OpJoinStaticStreamFlatMap)
            qIter = opExecutor.execute((OpJoinStaticStreamFlatMap) opJoin, input, vars, writer);
        else
            qIter = opExecutor.execute(opJoin, input, vars, writer);
        push(qIter);
    }

    @Override
    public void visit(OpUnion opUnion) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpDiff opDiff) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpMinus opMinus) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpConditional opCondition) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpSequence opSequence) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpDisjunction opDisjunction) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpList opList) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpOrder opOrder) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpReduced opReduced) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpDistinct opDistinct) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpSlice opSlice) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(OpTopN opTop) {
        // TODO Auto-generated method stub

    }
}
