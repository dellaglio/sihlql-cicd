/**
 *    The SihlQL Engine
 *
 *    Copyright 2020 University of Zurich
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package sihlmill.compiler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.jena.atlas.io.IndentedWriter;
import org.apache.jena.graph.Graph;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.StreamEndpoint0;
import org.apache.jena.query.StreamEndpoint1;
import org.apache.jena.query.StreamEndpoint2;
import org.apache.jena.sparql.algebra.Algebra;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.OpVisitorBase;
import org.apache.jena.sparql.algebra.op.OpJoin;
import org.apache.jena.sparql.algebra.walker.Walker;
import org.apache.jena.sparql.core.Var;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sihlmill.compiler.rewriter.J2FBase;
import sihlmill.compiler.rewriter.Vars;
import sihlmill.sihlql.rewriter.QLRewriter;

public final class Compiler {
	private static final Logger logger = LoggerFactory.getLogger(Compiler.class);

	public static Op compileQueryString(Query query) {
		final Op root = Algebra.compile(query);

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		root.output(new IndentedWriter(baos));
		logger.warn("The initial algebraic tree is \n {}", baos.toString());

		return new QLRewriter().rewrite(root);
	}

	public static File prepareOutputFile(String filePath) throws IOException {
		final File outputFile = new File(filePath);

		// create a new output file
		// if it already exists, delete it first
		if (outputFile.exists()) {
			outputFile.delete();
		}
		outputFile.createNewFile();

		return outputFile;
	}

	public static FileWriter prepareOutputWriter(File outputFile) throws IOException {
		// instantiate a writer for the output file
		// and fill it with initial static code
		final FileWriter writer = new FileWriter(outputFile);

		final List<String> staticImports = Files.readAllLines(Paths.get("src/main/resources/static_import"));
		writer.write(staticImports.stream().collect(Collectors.joining("\n")));
		writer.write("\n");

		return writer;
	}

	public static String generateTopology(String queryString, FileWriter writer, String latencyFile,
			boolean topologySmall) throws IOException {
		// ***
		// DATA SET UP
		// ***
		final List<Graph> input = DataCreator.createCompany();
		final Map<Integer, String> list = new HashMap<>();

		final Query q = QueryFactory.create(queryString);
		if (q.getPrivacy()) {
			logger.info("Differential privacy is enabled. Parameters are: epsilon = {}, w = {}, h0 = {}", q.getEpsilon(),
					q.getW(), q.getH0());
		} else {
			logger.info("Differential privacy is disabled.");
		}
		if (q.getOutputStreamEndpoints().size() > 0) {
			logger.info("Output endpoints: {}", q.getOutputStreamEndpoints());
		}

		J2FBase j2f = null;
		if (q.getPrivacy() && topologySmall) {
			j2f = new sihlmill.whole.small.wevent.compiler.J2F(q.getEpsilon(), q.getW(), q.getH0());
		} else if (!q.getPrivacy() && topologySmall) {
			// j2f = new J2FWithTimeAnnotationDisjoint();
			j2f = new sihlmill.whole.small.compiler.J2F();
		} else if (!q.getPrivacy() && !topologySmall) {
			j2f = new sihlmill.whole.big.compiler.J2F();
		} else {
			throw new RuntimeException("Not available yet");
		}

		for (final String s : j2f.getSpecificPackages()) {
			writer.write("import " + s + ";\n");
		}
		writer.write("\n");

		final List<String> staticHeader = Files.readAllLines(Paths.get("src/main/resources/static_header"));
		writer.write(staticHeader.stream().collect(Collectors.joining("\n")));
		writer.write("\n");

		// ***
		// TOPLOGY GOES HERE
		// ***
		// final StreamExecutionEnvironment env =
		// StreamExecutionEnvironment.getExecutionEnvironment();
		// env.setParallelism(1);

		final List<StreamEndpoint1> streams = q.getInputStreamEndpoints();
		if (streams.size() != 1) {
			throw new RuntimeException("Only queries with one stream are supported");
		}

		// DATA SETUP
		final StreamEndpoint1 inputStream = streams.get(0);
		System.out.println(inputStream.getType());
		switch (inputStream.getType()) {
		case Query.WEB:
			writer.write("final DataStream<TimestampedGraph> dataStream = env.addSource(new RemoteRDFStreamSourceFunction(\""
					+ inputStream.getURL() + "\"));\n");
			break;
		case Query.FILE:
			writer.write("final DataStream<TimestampedGraph> dataStream = env.addSource(new LocalRDFStreamSourceFunction(\""
					+ inputStream.getURL() + "\")).map(new JsonObjectToTimestampedGraph());\n");
			break;
		case Query.KAFKA:
			final StreamEndpoint2 is = (StreamEndpoint2) inputStream;
			logger.debug("Reading from kafka {}", is.toString());

			// generate kafka connection properties
			writer.write("Properties properties = new Properties();\n");
			writer.write("properties.setProperty(\"bootstrap.servers\", \"" + is.getURL() + "\");\n");
			writer.write("properties.setProperty(\"group.id\", \"sihlql\");\n");

			// add the topic as a variable
			writer.write("String topic = \"" + is.getTopic() + "\";\n");

			// create a datastream with kafka consumer
			// the string as of here contains JSON serialized to strings
			writer.write(
					"FlinkKafkaConsumer<String> kafkaConsumer = new FlinkKafkaConsumer<>(topic, new SimpleStringSchema(), properties);\n");
			writer.write("kafkaConsumer.setStartFromEarliest();");
			writer.write("final DataStream<String> kafkaStream = env.addSource(kafkaConsumer);\n");

			// map the stream to a deserialized version with timestamped graphs:
			writer.write(
					"final DataStream<TimestampedGraph> dataStream = kafkaStream.map(JSONSerializer::deserializeGraph);\n");
			break;
		case Query.MQTT:
			final StreamEndpoint2 mis = (StreamEndpoint2) inputStream;
			writer.write(
					"MqttConsumer mqttConsumer = new MqttConsumer(\""+mis.getURL()+"\", \""+mis.getTopic()+"\");\n");
			writer.write("final DataStream<String> mqttStream = env.addSource(mqttConsumer);\n");
			writer.write(
					"final DataStream<TimestampedGraph> dataStream = mqttStream.map(JSONSerializer::deserializeGraph);\n");
			break;
		default:
			writer.write(
					"final List<TimestampedGraph> input = " + DataCreator.createFunctionCall(inputStream.getURL()) + ";\n");
			writer.write("final DataStream<TimestampedGraph> dataStream = env.fromCollection(input);\n");
		}

		// compile the query string into an operations tree
		final Op root = compileQueryString(q);

		// Transformer.transform(new DPTransformer(), root);
		// root = Algebra.optimize(root);
		Walker.walk(root, new OpVisitorBase() {
			@Override
			public void visit(OpJoin opJoin) {
				logger.debug("The left son of the join operator is: {}", opJoin.getLeft().getClass());
				logger.debug("The right son of the join operator is: {}", opJoin.getRight().getClass());
			}
		});

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		root.output(new IndentedWriter(baos));
		logger.warn("The optimised algebraic tree is \n {}", baos.toString());

		// final Set<OpBGP> bgps = new HashSet<>();

		/*
		 * OpWalker.walk(root, new OpVisitorBase() {
		 *
		 * @Override public void visit(OpBGP opBGP) { bgps.add(opBGP); } });
		 *
		 *
		 * final List<Triple> triples = //
		 *
		 * bgps.size() > 0 ? bgps.iterator().next().getPattern().getList() // :
		 * null;
		 */

		final Map<Var, Integer> vars = new HashMap<>();
		vars.put(Var.alloc(".timestamp"), 0);

		final Vars vs = new Vars();
		vs.setVars(vars);

		final String stream = j2f.executeOp(root, "dataStream", vs, writer, q);

		final Set<StreamEndpoint0> outputs = q.getOutputStreamEndpoints();
		final Set<String> files = new HashSet<>();
		for (final StreamEndpoint0 endpoint : outputs) {
			System.out.println(endpoint);
			switch (endpoint.getType()) {
			case Query.STDOUT:
				writer.write(String.format("%s.print();\n", stream));
				break;
			case Query.KAFKA:
				final String uri = ((StreamEndpoint2) endpoint).getURL();
				final String topic = ((StreamEndpoint2) endpoint).getTopic();
				writer
				.write(String.format("%s.addSink(DataSinkBuilder.createKafkaSink(\"%s\", \"%s\"));\n", stream, uri, topic));
				break;
			case Query.MQTT:
				final String muri = ((StreamEndpoint2) endpoint).getURL();
				final String mtopic = ((StreamEndpoint2) endpoint).getTopic();
				writer
				.write(String.format("%s.addSink(new MqttSink(\"%s\", \"%s\"));\n", stream, muri, mtopic));
				break;
			case Query.FILE:
				final String filePath = ((StreamEndpoint1) endpoint).getURL();
				files.add(filePath);
				writer.write(String.format(
						"%s.map(new OutputFileSerializer()).writeAsText(\"%s\", org.apache.flink.core.fs.FileSystem.WriteMode.OVERWRITE);\n",
						stream, filePath));
				break;
			default:
				writer.write(String.format("%s.printStream();\n", stream));
				break;
			}
		}

		writer.write("try {\n");
		if (latencyFile != null) {
			writer.write("final long startTime = System.currentTimeMillis();\n");
		}
		writer.write("env.execute();\n");
		for (final String file : files) {
			final long timestamp = System.currentTimeMillis();
			writer.write("FileWriter fw" + timestamp + " = new FileWriter(new File(new URI(\"" + file + "\")), true);\n");
			writer.write("fw" + timestamp + ".write(\"]\");\n");
			writer.write("fw" + timestamp + ".close();\n");
		}
		if (latencyFile != null) {
			writer.write("final long endTime = System.currentTimeMillis();\n");
			writer.write("final long runTime = (endTime - startTime);\n");
			writer.write("final FileWriter latencyWriter = new FileWriter(new File(\"" + latencyFile + "\"), true);\n");
			writer.write("latencyWriter.append(runTime + \"\\n\");\n");
			writer.write("latencyWriter.close();\n");
		}
		writer.write("} catch (Exception e) {\n");
		writer.write("e.printStackTrace();\n");
		writer.write("}\n");
		writer.write("}\n");

		final List<String> staticFooter = Files.readAllLines(Paths.get("src/main/resources/static_footer"));
		writer.write(staticFooter.stream().collect(Collectors.joining("\n")));

		writer.close();

		return stream;
	}

	public static void main(String[] args) throws IOException {
		final ParameterTool param = ParameterTool.fromArgs(args);
		final String queryPath = param.get("query.path", null);
		final String latencyFile = param.get("latency.file.path", "latency-file.csv");
		final boolean topologySmall = Boolean.parseBoolean(param.get("topology.small", "true"));

		logger.debug("Now I will execute the query at {}", queryPath);

		final String query;
		if(queryPath != null) {
			final StringBuilder sb = new StringBuilder();
			Files.readAllLines(Paths.get(queryPath)).stream().filter(s -> !s.startsWith("#")).forEach(s -> sb.append(s + " "));
			query = sb.toString();
		} else {
			logger.error("No input query. A dummy topology has been generated");
			query = "PREFIX : <http://example.org/> " + 
					"PREFIX s: <https://schema.org/> " + 
					"SELECT ?s ?o " + 
					"FROM FILE STREAM <" + Paths.get(".").toAbsolutePath().normalize().toString() + "/src/main/resources/dummy-stream.jsonld> " + 
					"TO STDOUT " + 
					"WHERE{ " + 
					"  ?s ?p ?o . " + 
					"}";
			System.out.println(query);
		}

		// already contains all the static code (imports, etc.)
		final File folder = new File("src/main/java/sihlmill/generated");
		if (!folder.exists()) {
			folder.mkdir();
		}
		final File output = prepareOutputFile("src/main/java/sihlmill/generated/Topology.java");
		final FileWriter writer = prepareOutputWriter(output);

		// append the dynamically generated topology code to the output
		final String stream = generateTopology(query, writer, latencyFile, topologySmall);

		logger.debug("The generated stream is: {}", stream);
	}
}
