package sihlmill.io.mqtt;

import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeNoException;
import static org.junit.Assume.assumeTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.apache.flink.streaming.api.functions.sink.SinkFunction;

import static org.mockito.Mockito.anyString;


import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class MqttSinkTests {
	@BeforeAll
	public static void setupMqttConnection() {
		try {
			MqttClient client = new MqttClient("tcp://localhost:1883", "client"+System.currentTimeMillis());
			client.connect();
			
//			client.close();

			assumeTrue(true);
		} catch (MqttSecurityException e) {
			e.printStackTrace();
			assumeNoException(e);
		} catch (MqttException e) {
			assumeNoException(e);
			e.printStackTrace();
		}		
	}
	
	@Test
	public void injectionTest() {
		MqttSink mqttSink = new MqttSink("tcp://localhost:1883", "test");
		SinkFunction.Context<String> ctx = Mockito.mock(SinkFunction.Context.class);
		try {
			MqttClient client = new MqttClient("tcp://localhost:1883", "client");
			final Set<String> results = new HashSet<>();
			client.connect();
			client.setCallback(new MqttCallback() {
				
				@Override
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					results.add(message.toString());
				}
				
				@Override
				public void deliveryComplete(IMqttDeliveryToken token) {
				}
				
				@Override
				public void connectionLost(Throwable cause) {
					System.err.println("Connection lost");
					cause.printStackTrace();
				}
			});
			
			client.subscribe("test");
			mqttSink.invoke("test-writing", ctx);
			Thread.sleep(2000);
			assertEquals(1, results.size());
			assertEquals("test-writing", results.iterator().next());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
