package sihlmill.io.mqtt;

import static org.junit.Assume.assumeNoException;
import static org.junit.Assume.assumeTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.anyString;

import org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class MqttConnectorTests {
	@BeforeAll
	public static void setupMqttConnection() {
		try {
			MqttClient client = new MqttClient("tcp://localhost:1883", "client"+System.currentTimeMillis());
			client.connect();

			assumeTrue(true);
		} catch (MqttSecurityException e) {
			e.printStackTrace();
			assumeNoException(e);
		} catch (MqttException e) {
			assumeNoException(e);
			e.printStackTrace();
		}		
	}
	
	@Test
	public void injectionTest() {
		MqttConnector mqttConn = new MqttConnector("tcp://localhost:1883", "test");
		SourceContext<String> ctx = Mockito.mock(SourceContext.class);
		Set<String> messages = new HashSet<>();
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) {
				System.out.println(invocation.getArgument(0).toString());
				messages.add(invocation.getArgument(0).toString());
				return null;
			}
		})
		.when(ctx)
		.collect(anyString());

		try {
			MqttClient client = new MqttClient("tcp://localhost:1883", "client"+System.currentTimeMillis());
			client.connect();
			

			mqttConn.run(ctx);
			Thread.sleep(1000);
			MqttMessage msg = new MqttMessage("test-message".getBytes());
			client.publish("test", msg);
			Thread.sleep(1000);
			assertEquals(1, messages.size());
			assertEquals("test-message", messages.iterator().next());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
