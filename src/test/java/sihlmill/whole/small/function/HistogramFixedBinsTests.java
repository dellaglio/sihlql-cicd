package sihlmill.whole.small.function;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction.Context;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;
import static org.assertj.core.groups.Tuple.tuple;

import sihlmill.data.time.SingleTimeAnnotation;
import sihlmill.data.time.TimeAnnotation;


public class HistogramFixedBinsTests {
	@Test
	public void testHistogramStreamItemSuperset() {
		Node item0 = NodeFactory.createURI("http://example.org/item0");
		Node item1 = NodeFactory.createURI("http://example.org/item1");
		Node item2 = NodeFactory.createURI("http://example.org/item2");
		Node item3 = NodeFactory.createURI("http://example.org/item3");
		
		Set<Node> items = new HashSet<>();
		items.add(item0);
		items.add(item1);
		items.add(item2);
		items.add(item3);
		
		HistogramFixedBins hvb = new HistogramFixedBins<Tuple3<TimeAnnotation, Node, Double>, Tuple4<TimeAnnotation, Node, Double, Double>>(
				items,
				(Class<Tuple4<TimeAnnotation,Node,Double,Double>>)new Tuple4<TimeAnnotation,Node,Double,Double>().getClass(), 
				1);
		List<Tuple4<TimeAnnotation,Node,Double,Double>> out = new ArrayList<>();
		ListCollector<Tuple4<TimeAnnotation,Node,Double,Double>> collector = new ListCollector<Tuple4<TimeAnnotation,Node,Double,Double>>(out);

		TimeAnnotation t = new SingleTimeAnnotation(1l);

		List<Tuple3<TimeAnnotation,Node,Double>> in = new ArrayList<>();
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item1, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item2, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item2, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));

		TimeWindow tw = new TimeWindow(1l, 2l);
		Context contextMock = Mockito.mock(Context.class);
		when(contextMock.window()).thenReturn(tw);

		try {
			hvb.process(contextMock, in, collector);
			Assertions.assertThat(out)
			.hasSize(4)
			.extracting(n -> {return tuple(n.f1, n.f2, n.f3);})
			.containsExactlyInAnyOrder(
					tuple(item0, null, 0d), 
					tuple(item1, null, 1d), 
					tuple(item2, null, 2d),
					tuple(item3, null, 3d)
					)
			;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testHistogramStreamItemSubset() {
		Node item1 = NodeFactory.createURI("http://example.org/item1");
		Node item2 = NodeFactory.createURI("http://example.org/item2");
		Node item3 = NodeFactory.createURI("http://example.org/item3");
		
		Set<Node> items = new HashSet<>();
		items.add(item1);
		items.add(item2);
		
		HistogramFixedBins hvb = new HistogramFixedBins<Tuple3<TimeAnnotation, Node, Double>, Tuple4<TimeAnnotation, Node, Double, Double>>(
				items,
				(Class<Tuple4<TimeAnnotation,Node,Double,Double>>)new Tuple4<TimeAnnotation,Node,Double,Double>().getClass(), 
				1);
		List<Tuple4<TimeAnnotation,Node,Double,Double>> out = new ArrayList<>();
		ListCollector<Tuple4<TimeAnnotation,Node,Double,Double>> collector = new ListCollector<Tuple4<TimeAnnotation,Node,Double,Double>>(out);

		TimeAnnotation t = new SingleTimeAnnotation(1l);

		List<Tuple3<TimeAnnotation,Node,Double>> in = new ArrayList<>();
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item1, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item2, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item2, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));

		TimeWindow tw = new TimeWindow(1l, 2l);
		Context contextMock = Mockito.mock(Context.class);
		when(contextMock.window()).thenReturn(tw);

		try {
			hvb.process(contextMock, in, collector);
			Assertions.assertThat(out)
			.hasSize(2)
			.extracting(n -> {return tuple(n.f1, n.f2, n.f3);})
			.containsExactlyInAnyOrder(
					tuple(item1, null, 1d), 
					tuple(item2, null, 2d)
					)
			;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testHistogram3() {
		Node item1 = NodeFactory.createURI("http://example.org/item1");
		Node item2 = NodeFactory.createURI("http://example.org/item2");
		Node item3 = NodeFactory.createURI("http://example.org/item3");
		
		Set<Node> items = new HashSet<>();
		items.add(item1);
		items.add(item2);
		items.add(item3);
		
		HistogramFixedBins hvb = new HistogramFixedBins<Tuple3<TimeAnnotation, Node, Double>, Tuple4<TimeAnnotation, Node, Double, Double>>(
				items,
				(Class<Tuple4<TimeAnnotation,Node,Double,Double>>)new Tuple4<TimeAnnotation,Node,Double,Double>().getClass(), 
				1);
		List<Tuple4<TimeAnnotation,Node,Double,Double>> out = new ArrayList<>();
		ListCollector<Tuple4<TimeAnnotation,Node,Double,Double>> collector = new ListCollector<Tuple4<TimeAnnotation,Node,Double,Double>>(out);

		TimeAnnotation t = new SingleTimeAnnotation(1l);

		List<Tuple3<TimeAnnotation,Node,Double>> in = new ArrayList<>();
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item1, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item2, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item2, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));
		in.add(new Tuple3<TimeAnnotation,Node,Double>(t, item3, 1.0));

		TimeWindow tw = new TimeWindow(1l, 2l);
		Context contextMock = Mockito.mock(Context.class);
		when(contextMock.window()).thenReturn(tw);

		try {
			hvb.process(contextMock, in, collector);
			Assertions.assertThat(out)
			.hasSize(3)
			.extracting(n -> {return tuple(n.f1, n.f2, n.f3);})
			.containsExactlyInAnyOrder(
					tuple(item1, null, 1d), 
					tuple(item2, null, 2d), 
					tuple(item3, null, 3d))
			;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void testHistogram4() {
		Node item1 = NodeFactory.createURI("http://example.org/item1");
		Node item2 = NodeFactory.createURI("http://example.org/item2");
		Node item3 = NodeFactory.createURI("http://example.org/item3");
		
		Set<Node> items = new HashSet<>();
		items.add(item1);
		items.add(item2);
		items.add(item3);

		HistogramFixedBins hvb = new HistogramFixedBins<Tuple4<TimeAnnotation,Node,Node,Double>, Tuple5<TimeAnnotation,Node,Node,Double,Double>>(
				items,
				(Class<Tuple5<TimeAnnotation,Node,Node,Double,Double>>)new Tuple5<TimeAnnotation,Node,Node,Double,Double>().getClass(), 
				1);
		List<Tuple5<TimeAnnotation,Node,Node,Double,Double>> out = new ArrayList<>();
		ListCollector<Tuple5<TimeAnnotation,Node,Node,Double,Double>> collector = new ListCollector<Tuple5<TimeAnnotation,Node,Node,Double,Double>>(out);

		TimeAnnotation t = new SingleTimeAnnotation(1l);
		Node user1 = NodeFactory.createURI("http://example.org/user1");
		Node user2 = NodeFactory.createURI("http://example.org/user2");
		Node user3 = NodeFactory.createURI("http://example.org/user3");
		Node user4 = NodeFactory.createURI("http://example.org/user4");
		Node user5 = NodeFactory.createURI("http://example.org/user5");
		Node user6 = NodeFactory.createURI("http://example.org/user6");
		
		List<Tuple4<TimeAnnotation,Node,Node,Double>> in = new ArrayList<>();
		in.add(new Tuple4<TimeAnnotation,Node,Node,Double>(t, item1, user1, 1.0));
		in.add(new Tuple4<TimeAnnotation,Node,Node,Double>(t, item2, user2, 1.0));
		in.add(new Tuple4<TimeAnnotation,Node,Node,Double>(t, item3, user3, 1.0));
		in.add(new Tuple4<TimeAnnotation,Node,Node,Double>(t, item2, user4, 1.0));
		in.add(new Tuple4<TimeAnnotation,Node,Node,Double>(t, item3, user5, 1.0));
		in.add(new Tuple4<TimeAnnotation,Node,Node,Double>(t, item3, user6, 1.0));

		TimeWindow tw = new TimeWindow(1l, 2l);
		Context contextMock = Mockito.mock(Context.class);
		when(contextMock.window()).thenReturn(tw);

		try {
			hvb.process(contextMock, in, collector);
			Assertions.assertThat(out)
			.hasSize(3)
			.extracting(n -> {return tuple(n.f1, n.f2, n.f3, n.f4);})
			.containsExactlyInAnyOrder(
					tuple(item1, null, null, 1d), 
					tuple(item2, null, null, 2d), 
					tuple(item3, null, null, 3d))
			;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
