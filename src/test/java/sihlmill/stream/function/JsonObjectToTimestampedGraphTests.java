package sihlmill.stream.function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.datatypes.xsd.impl.XSDDouble;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.graph.GraphFactory;
import org.apache.jena.vocabulary.XSD;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.groups.Tuple.tuple;

import sihlmill.data.time.TimeAnnotation;


public class JsonObjectToTimestampedGraphTests {

	@Test
	public void testGraphToModel() throws Exception {
		String input = "{\"@graph\":[{\"@id\":\"http://example.org/ratingA1BQ8UOASGLQSP-B0063IH60K\",\"https://schema.org/ratingValue\":5.0},{\"@id\":\"http://example.org/reviewA1BQ8UOASGLQSP-B0063IH60K\",\"author\":\"http://example.org/userA1BQ8UOASGLQSP\",\"itemReviewed\":\"http://example.org/itemB0063IH60K\",\"reviewRating\":\"http://example.org/ratingA1BQ8UOASGLQSP-B0063IH60K\"}],\"@context\":{\"ratingValue\":{\"@id\":\"https://schema.org/ratingValue\",\"@type\":\"http://www.w3.org/2001/XMLSchema#double\"},\"reviewRating\":{\"@id\":\"https://schema.org/reviewRating\",\"@type\":\"@id\"},\"author\":{\"@id\":\"https://schema.org/author\",\"@type\":\"@id\"},\"itemReviewed\":{\"@id\":\"https://schema.org/itemReviewed\",\"@type\":\"@id\"}}, \"@id\":\"_:01134777600\", \"http://www.w3.org/ns/prov#generatedAtTime\":\"1134777600\"}";

		Node rating = NodeFactory.createURI("http://example.org/ratingA1BQ8UOASGLQSP-B0063IH60K");
		Node review = NodeFactory.createURI("http://example.org/reviewA1BQ8UOASGLQSP-B0063IH60K");
		Node author = NodeFactory.createURI("http://example.org/userA1BQ8UOASGLQSP");
		Node item = NodeFactory.createURI("http://example.org/itemB0063IH60K");

		Node ratingValue = NodeFactory.createURI("https://schema.org/ratingValue");
		Node authorValue = NodeFactory.createURI("https://schema.org/author");
		Node itemReviewed = NodeFactory.createURI("https://schema.org/itemReviewed");
		Node reviewRating = NodeFactory.createURI("https://schema.org/reviewRating");

		Set<Triple> g = new HashSet<>();
		g.add(Triple.create(rating, ratingValue, NodeFactory.createLiteral(Double.toString(5.0)+"E0", XSDDouble.XSDdouble)));
		g.add(Triple.create(review, authorValue, author));
		g.add(Triple.create(review, itemReviewed, item));
		g.add(Triple.create(review, reviewRating, rating));

		JsonObjectToTimestampedGraph jottg = new JsonObjectToTimestampedGraph();
		Assertions.assertThat(jottg.map(input).getGraph().find().toSet())
		.containsExactlyInAnyOrderElementsOf(g)
		;
	}

}
