package sihlmill.stream.function;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction.Context;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.datatypes.xsd.impl.XSDDouble;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.RDFParserBuilder;
import org.apache.jena.riot.lang.JsonLDReader;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.graph.GraphFactory;
import org.apache.jena.vocabulary.XSD;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.groups.Tuple.tuple;
import static org.mockito.Mockito.when;

import sihlmill.data.time.SingleTimeAnnotation;
import sihlmill.data.time.TimeAnnotation;


public class OutputConstructSerializerTests {

	@Test
	public void testTupleToJsonLD() throws Exception {
			String input = "{\"@graph\":[{\"@id\":\"http://example.org/ratingA1BQ8UOASGLQSP-B0063IH60K\",\"https://schema.org/ratingValue\":5.0},{\"@id\":\"http://example.org/reviewA1BQ8UOASGLQSP-B0063IH60K\",\"author\":\"http://example.org/userA1BQ8UOASGLQSP\",\"itemReviewed\":\"http://example.org/itemB0063IH60K\",\"reviewRating\":\"http://example.org/ratingA1BQ8UOASGLQSP-B0063IH60K\"}],\"@context\":{\"ratingValue\":{\"@id\":\"https://schema.org/ratingValue\",\"@type\":\"http://www.w3.org/2001/XMLSchema#double\"},\"reviewRating\":{\"@id\":\"https://schema.org/reviewRating\",\"@type\":\"@id\"},\"author\":{\"@id\":\"https://schema.org/author\",\"@type\":\"@id\"},\"itemReviewed\":{\"@id\":\"https://schema.org/itemReviewed\",\"@type\":\"@id\"}}, \"@id\":\"_:01134777600\", \"http://www.w3.org/ns/prov#generatedAtTime\":\"1134777600\"}";

			Node item0 = NodeFactory.createURI("http://example.org/item0");
			Node item1 = NodeFactory.createURI("http://example.org/item1");
			Node item2 = NodeFactory.createURI("http://example.org/item2");
			Node item3 = NodeFactory.createURI("http://example.org/item3");

			Node user0 = NodeFactory.createURI("http://example.org/user0");
			Node user1 = NodeFactory.createURI("http://example.org/user1");

			Node likes = NodeFactory.createURI("http://example.org/likes");
			Node dislikes = NodeFactory.createURI("http://example.org/dislikes");

			List<Triple> triplePatterns = new ArrayList<>();
			triplePatterns.add(Triple.create(Var.alloc("s"), likes, Var.alloc("o")));

			Map<Var, Integer> vars = new HashMap<>();
			vars.put(Var.alloc(".timestamp"), 0);
			vars.put(Var.alloc("s"), 1);
			vars.put(Var.alloc("o"), 2);
			
			TimeAnnotation t = new SingleTimeAnnotation(2l);
			
			Set<Tuple3<TimeAnnotation, Node, Node>> winContent = new HashSet<>();
			winContent.add(new Tuple3<>(t, user0, item0));
			winContent.add(new Tuple3<>(t, user1, item1));
			winContent.add(new Tuple3<>(t, user1, item2));
			
			OutputConstructSerializer<Tuple3<TimeAnnotation, Node, Node>> ocs = new OutputConstructSerializer<>(triplePatterns, vars, "http://example.org/stream");
			
			TimeWindow tw = new TimeWindow(1l, 2l);
			
			Context contextMock = Mockito.mock(Context.class);
			when(contextMock.window()).thenReturn(tw);

			String out = ocs.generateElement(contextMock, winContent);
			Assertions.assertThat(out)
			.isEqualTo("{\"@graph\":[{\"@id\":\"http://example.org/user0\",\"likes\":\"http://example.org/item0\"},{\"@id\":\"http://example.org/user1\",\"likes\":[\"http://example.org/item2\",\"http://example.org/item1\"]}],\"@context\":{\"likes\":{\"@id\":\"http://example.org/likes\",\"@type\":\"@id\"}}, \"@id\":\"http://example.org/stream1\", \"http://www.w3.org/ns/prov#generatedAtTime\":\"1\"}");
	}

	@Test
	public void testEmptyOutput() throws Exception {
			List<Triple> triplePatterns = new ArrayList<>();
			Node likes = NodeFactory.createURI("http://example.org/likes");
			triplePatterns.add(Triple.create(Var.alloc("s"), likes, Var.alloc("o")));

			Map<Var, Integer> vars = new HashMap<>();
			vars.put(Var.alloc(".timestamp"), 0);
			vars.put(Var.alloc("s"), 1);
			vars.put(Var.alloc("o"), 2);
			
			TimeAnnotation t = new SingleTimeAnnotation(2l);
			
			Set<Tuple3<TimeAnnotation, Node, Node>> winContent = new HashSet<>();
			
			OutputConstructSerializer<Tuple3<TimeAnnotation, Node, Node>> ocs = new OutputConstructSerializer<>(triplePatterns, vars, "http://example.org/stream");
			
			TimeWindow tw = new TimeWindow(1l, 2l);
			
			Context contextMock = Mockito.mock(Context.class);
			when(contextMock.window()).thenReturn(tw);

			String out = ocs.generateElement(contextMock, winContent);
			Assertions.assertThat(out)
			.isEqualTo("{\"@graph\":[], \"@id\":\"http://example.org/stream1\", \"http://www.w3.org/ns/prov#generatedAtTime\":\"1\"}");
	}
}
