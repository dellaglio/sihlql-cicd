package sihlmill.stream.function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.graph.GraphFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.groups.Tuple.tuple;

import sihlmill.data.time.TimeAnnotation;


public class GraphToModelTests {

	@Test
	public void testGraphToModel() throws Exception {
		Node item0, item1, item2, item3;
		Node user0, user1;
		Node likes, dislikes;
		Graph g;

		item0 = NodeFactory.createURI("http://example.org/item0");
		item1 = NodeFactory.createURI("http://example.org/item1");
		item2 = NodeFactory.createURI("http://example.org/item2");
		item3 = NodeFactory.createURI("http://example.org/item3");

		user0 = NodeFactory.createURI("http://example.org/user0");
		user1 = NodeFactory.createURI("http://example.org/user1");

		likes = NodeFactory.createURI("http://example.org/likes");
		dislikes = NodeFactory.createURI("http://example.org/dislikes");

		g = GraphFactory.createGraphMem();
		g.add(Triple.create(user0, likes, item0));
		g.add(Triple.create(user1, likes, item0));
		g.add(Triple.create(user0, likes, item1));
		g.add(Triple.create(user1, dislikes, item1));
		g.add(Triple.create(user0, dislikes, item2));
		g.add(Triple.create(user1, likes, item3));

		GraphToModel g2m = new GraphToModel();
		Assertions.assertThat(g2m.map(g))
		.isInstanceOf(Model.class)
		.extracting(m -> m.getGraph())
		.isEqualTo(g);
	}

}
