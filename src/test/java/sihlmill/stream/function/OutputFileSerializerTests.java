package sihlmill.stream.function;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction.Context;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.datatypes.xsd.impl.XSDDouble;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.RDFParserBuilder;
import org.apache.jena.riot.lang.JsonLDReader;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.graph.GraphFactory;
import org.apache.jena.vocabulary.XSD;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.groups.Tuple.tuple;
import static org.mockito.Mockito.when;

import sihlmill.data.time.SingleTimeAnnotation;
import sihlmill.data.time.TimeAnnotation;


public class OutputFileSerializerTests {

	@Test
	public void testConnectors() throws Exception {
		OutputFileSerializer ofs = new OutputFileSerializer();
		
		String out = ofs.map("first");
		Assertions.assertThat(out).isEqualTo("[\nfirst");
		out = ofs.map("second");
		Assertions.assertThat(out).isEqualTo(",\nsecond");
	}
}
