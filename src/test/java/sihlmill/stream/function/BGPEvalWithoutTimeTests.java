package sihlmill.stream.function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.jena.datatypes.TimestampedGraph;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.graph.GraphFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.groups.Tuple.tuple;

import sihlmill.data.time.TimeAnnotation;


public class BGPEvalWithoutTimeTests {
	static Node item0, item1, item2, item3;
	static Node user0, user1;
	static Node likes, dislikes;
	static Model model;

	@BeforeAll
	public static void beforeAll() {
		item0 = NodeFactory.createURI("http://example.org/item0");
		item1 = NodeFactory.createURI("http://example.org/item1");
		item2 = NodeFactory.createURI("http://example.org/item2");
		item3 = NodeFactory.createURI("http://example.org/item3");

		user0 = NodeFactory.createURI("http://example.org/user0");
		user1 = NodeFactory.createURI("http://example.org/user1");

		likes = NodeFactory.createURI("http://example.org/likes");
		dislikes = NodeFactory.createURI("http://example.org/dislikes");

		Graph g = GraphFactory.createGraphMem();
		g.add(Triple.create(user0, likes, item0));
		g.add(Triple.create(user1, likes, item0));
		g.add(Triple.create(user0, likes, item1));
		g.add(Triple.create(user1, dislikes, item1));
		g.add(Triple.create(user0, dislikes, item2));
		g.add(Triple.create(user1, likes, item3));
		
		model = ModelFactory.createModelForGraph(g);
	}

	@Test
	public void testBGPOneVar() {
		List<Triple> triplePatterns = new ArrayList<>();
		triplePatterns.add(Triple.create(Var.alloc("s"), dislikes, item1));

		Map<Var, Integer> vars = new HashMap<>();
		vars.put(Var.alloc("s"), 0);

		BGPEvalWithoutTime<Tuple1<Node>> bewt = new BGPEvalWithoutTime<>(triplePatterns, vars);

		List<Tuple1<Node>> out = new ArrayList<>();
		ListCollector<Tuple1<Node>> collector = new ListCollector<Tuple1<Node>>(out);
		try {
			bewt.flatMap(model, collector);

			Assertions.assertThat(out)
			.hasSize(1)
			.extracting(n -> {return tuple(n.f0);})
			.containsExactlyInAnyOrder(
					tuple(user1) 
					)
			;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testBGPTwoVars() {
		List<Triple> triplePatterns = new ArrayList<>();
		triplePatterns.add(Triple.create(Var.alloc("s"), dislikes, Var.alloc("o")));

		Map<Var, Integer> vars = new HashMap<>();
		vars.put(Var.alloc("s"), 0);
		vars.put(Var.alloc("o"), 1);

		BGPEvalWithoutTime<Tuple2<Node,Node>> bewt = new BGPEvalWithoutTime<>(triplePatterns, vars);

		List<Tuple2<Node,Node>> out = new ArrayList<>();
		ListCollector<Tuple2<Node,Node>> collector = new ListCollector<>(out);
		try {
			bewt.flatMap(model, collector);

			Assertions.assertThat(out)
			.hasSize(2)
			.extracting(n -> {return tuple(n.f0, n.f1);})
			.containsExactlyInAnyOrder(
					tuple(user1, item1),
					tuple(user0, item2)
					)
			;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testBGPNoResults() {
		List<Triple> triplePatterns = new ArrayList<>();
		triplePatterns.add(Triple.create(Var.alloc("s"), dislikes, item0));

		Map<Var, Integer> vars = new HashMap<>();
		vars.put(Var.alloc("s"), 0);

		BGPEvalWithoutTime<Tuple1<Node>> bewt = new BGPEvalWithoutTime<>(triplePatterns, vars);

		List<Tuple1<Node>> out = new ArrayList<>();
		ListCollector<Tuple1<Node>> collector = new ListCollector<>(out);
		try {
			bewt.flatMap(model, collector);

			Assertions.assertThat(out)
			.hasSize(0)
			;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
